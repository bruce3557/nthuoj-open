<h3><{ $contest_name }></h3>
<hr>
<div align="center">

<h4 align="left">Clarification</h4>
<table class="table table-striped" style="width: 80%">
  <tr>
    <th>Clarification ID</th>
    <th>Problem ID</th>
    <th>Title</th>
    <th>Message</th>
    <th>Time</th>
    <th>Reply</th>
    <{if $is_admin }>
      <th>&nbsp;</th>
    <{/if}>
  </tr>

  <{section name=id loop=$clarification }>
    <tr>
      <td><{ $clarification[id].clid }></td>
      <td>
      <{if $clarification[id].pid eq 0 }>
        General
      <{else}>
        <{ $clarification[id].pid }>
      <{/if}>
      </td>
      <td><{ $clarification[id].title }></td>
      <td><{ $clarification[id].msg }></td>
      <td><{ $clarification[id].time }></td>
      <td><{ $clarification[id].reply }></td>
      <{if $is_admin }>
        <!--<td><button class="btn">Reply</button></td>-->
        <td>
          <a href="#replyClarification<{ $clarification[id].clid }>" role="button" class="btn" data-toggle="modal">Reply</a>
          <div id="replyClarification<{ $clarification[id].clid }>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
              <h3 id="myModalLabel">Reply <{ $clarification[id].clid }></h3>
            </div>
            
            <form method="post" action="reply_clarification.php?clid=<{ $clarification[id].clid }>" enctype="multipart/form-data">
              <div class="modal-body">
                <table class="table">
                  <tr>
                    <th>Message:</th>
                    <td><{ $clarification[id].msg }></td>
                  </tr>
                  <tr>
                    <th>Reply:</th>
                    <td><textarea name="reply<{ $clarification[id].clid }>"></textarea></td>
                  </tr>
                </table>
              </div>
              <div class="modal-footer">
                <input type="submit" value="Submit" name="submit" class="btn btn-primary" />
              </div>
            </form>
          </div>
        </td>
      <{/if}>
    </tr>
  <{/section}>
</table>

<{if ($is_login and $is_running) or $is_admin }>
  <a href="#sendClarification" role="button" class="btn btn-primary" data-toggle="modal">Send Clarification</a>
  <div id="sendClarification" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
      <h3 id="myModalLabel">Send Clarification</h3>
    </div>

    <form method="post" action="send_clarification.php" enctype="multipart/form-data">
      <div class="modal-body">
        <!--<p>One fine body...</p>-->
        <table class="table">
          <tr>
            <th>Problem ID</th>
            <td><select name="pid">
              <!---<option value="general">General</option>-->
              <{section name=id loop=$rs }>
                <option value="<{ $rs[id].pid }>"><{ $rs[id].pid }></option>
              <{/section}>
            </select></td>
          </tr>
          <tr>
            <th>Title</th>
            <td><input type="text" maxlength="100" name="title" /></td>
          </tr>
          <tr>
            <th>Message</th>
            <td><textarea maxlength="1000" name="msg" rows=10 cols=20 /></textarea></td>
          </tr>
        </table>
        <input name="cid" type="hidden" value="<{ $cid }>"/>
      </div>
      <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <input type="submit" value="Send" name="submit" class="btn btn-primary" />
      </div>
    </form>
  </div>
<{/if}>

<br><hr><br>
<h4 align="left">Problem List</h4>
<table class="table table-striped">
  <tr>
    <th width="20%">Problem ID</th>
    <th>Title</th>
	<!---<th>Sample Input</th>
	<th>Sample Output</th>-->
  </tr>

  <{section name=id loop=$rs }>
  <{ if $rs[id].accepted }>
    <tr class="success">
  <{ else }>
    <tr>
  <{ /if }>
      <td><{$rs[id].pid}></td>
      <td><a href=problem.php?hash=<{$rs[id].hashcode}> ><{$rs[id].valuetext}></a></td>
	  <!---<td><a href=testdata2.php?siid=<{$rs[id].pid}>.in ><{$rs[id].pid}>.in</a></td>
	  <td><a href=testdata2.php?soid=<{$rs[id].pid}>.out><{$rs[id].pid}>.out</a></td>-->
    </tr>
  <{/section}>

</table>
</div>
