<{if $findUser eq 1 }>
  <form method="post" name="findUser">
  User ID: <input name="uid"></input>
  
  </form>
<{/if}>
<h3>Personal Status</h3>
<div align="center">
<table class="table table-striped">
  <tr>
    <th>Submission ID</th>
    <th>Date</th>
    <th>User ID</th>
    <th>Problem ID</th>
    <th>Status</th>
    <th>CPU</th>
    <th>Source</th>
  </tr>

  <{section name=id loop=$rs}>
    <tr>
    <{if ($rs[id].color eq '"red"') }>
    <tr class="success">
    <{elseif ($rs[id].color eq '"blue"') }>
    <tr class="info">
    <{elseif ($rs[id].color eq '"green"') }>
    <tr class="error">
    <{else}>
    <tr class="warning">
    <{/if}>
      <td>
      <{ if $show_detail }>
        <a href=submission_details.php?sid=<{ $rs[id].sid }>><{ $rs[id].sid }></a>
      <{ else }>
        <{ $rs[id].sid }>
      <{ /if }>
      </td>
      <td><{ $rs[id].date }></td>
      <td><{ $rs[id].uid }></td>
      <td><a href=problem.php?pid=<{ $rs[id].pid }> ><{ $rs[id].pid }> - <{ $rs[id].pname }></a></td>
      
      <td><b><font color=<{$rs[id].color}> >
      <{if ($rs[id].status eq "Compile Error") && $is_login and ($is_admin or ($suid eq $rs[id].uid)) }>
        <a href=err_msg.php?sid=<{$rs[id].sid}> ><font color="blue"><{$rs[id].status}></font></a>
      <{else}>
        <{$rs[id].status}>
      <{/if}>
      </font></b></td>
      
      <td>
        <{if $rs[id].cpu }>
          <{ $rs[id].cpu }>
        <{else}>
          &nbsp;
        <{/if}>
      </td>

      <{if $is_login and $is_admin }>
        <td><a href=testdata.php?src=<{ $rs[id].sid }>.<{ $rs[id].source }>><{ $rs[id].sourceone }></a></td>
      <{else}>
        <td><{ $rs[id].sourceone }></td>
      <{/if}>

    </tr>
  <{/section}>
</table></div>

<p style="text-align: center"> <{ $first_page }> <{ $prev_page }> <{ $next_page }> <{ $last_page }> </p>
