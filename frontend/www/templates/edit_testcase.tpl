<br>
<h3>Edit Testcase PID: <{$pid}> 
<p><{$msg}></p>
</h3>
	
	<table class="edit_tc table table-striped" >
      <tr>
        <th>
			Testcase Number
        </th>
		<th />
        <th>
			Time Limit
		</th>
		<th>
			Memory Limit
		</th>
		<th>
		</th>
		<th></th>
	  </tr>
	  <tr></tr>
    <form name="delete-button" method="POST" enctype="multipart/form-data" > 
		<{section name=id loop=$rs }>
			<div align="center">
			  <tr>
				<td>
				  <a href="./testdata2.php?tid=<{$rs[id].testcaseId}>.in"><{$rs[id].testcaseId}>.in </a>
				</td>
				<td>
				  <a href="./testdata2.php?tid=<{$rs[id].testcaseId}>.out"> <{$rs[id].testcaseId}>.out </a>
				</td>				
				<td>
				  <input name = "timeLimit<{$rs[id].testcaseId}>" type="number" value =<{$rs[id].timeLimit}> />
				</td>
				<td>
				  <input name = "memoryLimit<{$rs[id].testcaseId}>" type="number" value=<{$rs[id].memoryLimit}> />
				</td>
				<td>
					<input type="button" value="Update" class="btn btn-primary"  name="button<{$rs[id].testcaseId}>" id="<{$rs[id].testcaseId}>" class="update-button" onclick=update_testcase(<{$rs[id].testcaseId}>); />
				</td>
				<td>
					<input type="button" value="Delete" class="btn btn-primary"  name="button<{$rs[id].testcaseId}>" id="<{$rs[id].testcaseId}>" class="delete-button" onclick=delete_testcase(<{$rs[id].testcaseId}>); />
				</td>
			  </tr>
			</div>
		<{/section}>
	</form>
	<tr><td /><td /><td /><td /><td /><td /></tr>
	<tr></tr>
	<form name="newTestcase" method="post" enctype="multipart/form-data" >
		<tr>
			<th>Input</th>
			<th>Output</th>
			<th>TimeLimit</th>
			<th>MemoryLimit</th>
			<th></th>
			<th></th>
		</tr>
		<tr>
			<td>
			   <input type="file" name="inputFile" id ="inputFile" size=16 />
			</td>
			<td>
				<input type="file" name="outputFile" id ="outputFile" size=16 />
			</td>
			<td>
				<input type ="number" name ="timeLimit" id="timeLimit" min="1" />
			</td>
			<td>
				<input type ="number" name="memoryLimit" id="memoryLimit" min="1"/>
			</td>
			<td />
			<td>
				<input type ="submit" value="新增" onClick="return checkform();" class="btn btn-primary" />
			</td>
			
		</tr>
	</form>
	</table>

<script language="javascript">
	console.log('HIIIII');
	
	function checkform(){
		if(document.newTestcase.inputFile.value==""){
			alert("Input File Empty");
			return false;
		}else if(document.newTestcase.outputFile.value==""){
			alert("Output File Empty");
			return false;
		}else if(document.newTestcase.timeLimit.value==""){
			alert("Time Limit Empty");
			return false;
		}else if(document.newTestcase.memoryLimit.value==""){
			alert("Memory Limit Empty");
			return  false;
		}
		return true;
	}
	function delete_testcase(buttonId){
		if(!confirm("確定要刪除Test Case?")) return false;
		var xhr;		 
			if(window.ActiveXObject){
				xhr = new ActiveXObject("Microsoft.XMLHTTP");
			}
			else if(window.XMLHttpRequest){
				xhr = new XMLHttpRequest();
			}
			//alert(buttonId);
				
			
			//alert(args);
				xhr.open("GET","delete_testcase.php?tid="+buttonId);
				xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded;");
				xhr.send();				
			
			xhr.onreadystatechange = function(){
				if(xhr.readyState==4){
					if(xhr.status>=200 && xhr.status<300){						
						result =xhr.responseText;
						alert(result);
						location.reload();
						location.reload();
						location.reload();
					}else {
						alert("OAO~ Something Wrong!");
					}
				 
				}
			
			}

	}
	function update_testcase(buttonId){
		if(!confirm("Are you sure you want to update testcase?")) return false;
		var xhr;		 
			if(window.ActiveXObject){
				xhr = new ActiveXObject("Microsoft.XMLHTTP");
			}
			else if(window.XMLHttpRequest){
				xhr = new XMLHttpRequest();
			}
			//alert(buttonId);
			timeLimit = document.getElementsByName("timeLimit"+buttonId)[0]["value"];
			memoryLimit = document.getElementsByName("memoryLimit"+buttonId)[0]["value"];
			//alert(args);
				xhr.open("GET","update_testcase.php?tid="+buttonId+"&timeLimit="+timeLimit+"&memoryLimit="+memoryLimit);
				xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded;");
				xhr.send();				
			
			xhr.onreadystatechange = function(){
				if(xhr.readyState==4){
					if(xhr.status>=200 && xhr.status<300){						
						result =xhr.responseText;
						alert(result);
						
					}else {
						alert("OAO~ Something Wrong!");
					}
				 
				}
			
			}

	}
</script>


<head> 
  <style type="text/css">
		input[type="file"]
		{
			width:15em;
		}
		input[type="number"]
		{
			height:2em;
			width:6em;
		}
  </style>
</head>