<nav id="nav">
	<ul>
		<li><a href="#" onclick="clickBar('trans');" >Format Transformation</a></li>
		<li><a href="#" onclick="clickBar('basic');" >Basic Info</a></li>
		<{section name=i loop=$rs }>
			<li>
			<a href="#<{ $rs[i].title }>" onclick="clickBar('<{ $rs[i].title }>');">
				<{ $rs[i].title }>
			</a>
			</li>
		<{/section}>
		<li>
			<a href="#" onclick="clickBar('tc_upload')">
				TestCase Upload
			</a>
		</li>
		<li><a href="#" onclick="saveOnClick();" name="submit" >Save</a></li>
		<!--<li><a href="#" onclick="clickBar('view');" >View</a></li>-->
	</ul>
</nav>

<!---->
<form id="form1" method = "POST" enctype = "multipart/form-data">
<div class="basic" id="basic">
<table class="table table-striped">
<tr>
	<h2><{ $pid }> - <{ $pname }> [<a href=problem.php?pid=<{ $pid }> target="problem" >View</a>] </h2>
</tr>

<tr>
  <th>Problem Name: &nbsp;</th>
  <td><input name = 'pname' type = "text" size = "32" maxlength = "64" value = "<{ $pname }>"  /><br /></td>
</tr>


<tr>
  <th>Parent PID: &nbsp;</th>
  <td><input name = 'parent_pid' type = "text" size = "32" maxlength = "64" value = "<{ $parent_pid }>"  /><br /></td>
</tr>

<tr>
  <th>Problemsetter: &nbsp;</th>
  <td><input name ='problemsetter' type ="text" size = "16" maxlength = "32" value = "<{ $problemsetter }>" /></td>
</tr>

<tr>
  <th>Anonymous: &nbsp;</th>
  <td><input name="anonymous" type = "checkbox" value = "checked" <{ $anonymous }>  > <br /></td>

</tr>

<tr>
  <th>Visible: &nbsp;</th>
  <td><input name="visible" type = "checkbox" value = "checked" <{ $visible }>  > <br /></td>

</tr>

<tr>
  <th>Judge Source:&nbsp;</th>
  <td>
      <select name="tid">
        <option value='0' <{if $tid==0 }>selected='selected'<{/if}> >Local (Default)</option>
        <option value='1' <{if $tid==1 }>selected='selected'<{/if}> >ICPC</option>
        <option value='2' <{if $tid==2 }>selected='selected'<{/if}>>POJ</option>
        <option value='3' <{if $tid==3 }>selected='selected'<{/if}> >UVA</option>
        <option value='4' <{if $tid==4 }>selected='selected'<{/if}> >ZOJ</option>
      </select>
  </td>
</tr>


<tr>
  <th>Real ID:&nbsp;</th>
  <td><input name = 'rid' type = "text" size = "4" maxlength = "10" value = <{ $realid }> ><br /></td>
</tr>

<tr>
  <th>Problem Page:&nbsp;</th>
  <td><input name='page_link' type="text" size="10" maxlength="1000" value= <{ $page_link }> ><br /></td>
</tr>

<tr>
  <th>Special Judge:</th>
  <td>
    <input type = "radio" value = "" name = "special_judge"  <{if $jdg eq "" }>checked<{/if}> /> No 
    <input type = "radio" value = "c" name = "special_judge" <{if $jdg eq "c" }>checked<{/if}> /> C 
    <input type = "radio" value = "cpp" name = "special_judge" <{if $jdg eq "cpp" }>checked<{/if}> /> C++ <br />  
  </td>
</tr>

<tr>
  <th>Judge Program: <{ $jdg_link }></th>
  <td><input type= "file" name="judge_program" size="16" /> <br /> 
  The judge program must take the judge input file of the testdata as arg1, and the output file of users as arg2. <br /> Then it should write the result to stdout, which will then be compared with the output file uploaded. <br /></td>
</tr>

</table>
</div>

 <{section name=id loop=$rs }>
    <div id="<{ $rs[id].title }>" class="bar" style="display:none;">
		<div colspan=2 class="barForm">
			<h4><{ $rs[id].title }></h4><br>
			<{ $rs[id].content }>
		</div>
	</div>
  <{/section}>

<tr><td>
  <input type = "text" value = "Save" name = "hasSubmit" style="display:none;"/>
</td></tr>

</form>



<!---->

<div id="trans" style="display:none" class="bar">
	  <form name="myform" class="myform">
		<h3>Format Transformation</h3><br>
		Times New Roman : <br>
		<textarea name="in1" cols=60 rows=20 id="text3" onChange="trans2();"></textarea>
		<textarea name="out1" cols=60 rows=20 id="text4"></textarea><br>
		
        Courier New : <br>
        <textarea name="in1" cols=60 rows=20 id="text1" onChange="trans1();"></textarea>
        <textarea name="out1" cols=60 rows=20 id="text2"></textarea><br>
	  </form>
</div>

<!------>
<div class="bar" id="tc_upload" style="display:none;"></div>
<div class="bar" id="view" style="display:none;"></div>

<script>
function trans1() {
  text1 = document.getElementById("text1");
  text2 = document.getElementById("text2");
  text2.value = '<p><span style="font-size: large; "><span style="font-family:'+" 'Courier New'; "+'">'
    + text1.value.replace(/\n/gi, "<br />") + "</p>";
}

function trans2() {
  text1 = document.getElementById("text3");
  text2 = document.getElementById("text4");
  text2.value = '<p><span style="font-size: large; "><span style="font-family:'+" 'Times New Roman'; "+'">'
    + text1.value.replace(/\n/gi, "<br />") + "</p>";
}
function clickBar(str){
	var el = document.getElementById(str);
	if( el.style.display == "none" ){
		hideClass("bar");
		hideClass("basic");
		show(str);
	}
	
	if( str=="view" ){
		el.innerHTML = " <iframe src='problem.php?pid=<{$pid}>&view=1'></iframe> ";
	}
	else if( str=="tc_upload" )	{
		el.innerHTML = " <iframe src='edit_testcase.php?pid=<{$pid}>'></iframe> "; 
	}
}

function show(str) {		
	var el = document.getElementById(str);
	el.style.display = "";
}
function hide(str) {		
	var el = document.getElementById(str);
	el.style.display = "none";
}
function hideClass(str) {		
	var arr = document.getElementsByClassName(str);
	var i;
	for(i=0 ; i<arr.length ; i++ ){
		arr[i].style.display = "none";
	}
}
function saveOnClick(){
	var el = document.getElementById('form1');
	el.submit();
}
</script>

<!---CSS CODE--->
<head> 
  <style type="text/css">
	iframe
	{
		//background: rgba(255,255,250,1);
		color: black;
			position: relative !important;
			width:75%;
			height:80%;
			top:4.5em;
			left:5%;
		
		margin-top: 20px;
		margin-bottom: 30px;

		-moz-border-radius: 12px;
		-webkit-border-radius: 12px; 
		border-radius: 12px; 

		-moz-box-shadow: 4px 4px 14px #000; 
		-webkit-box-shadow: 4px 4px 14px #000; 
		box-shadow: 4px 4px 14px #000; 
		
	}
	.basic
	{
		//background: rgba(255,255,250,1);
		text-align: left;
		//position: fixed;
		overflow-y:auto;
		//top: 20%;
		//left: 50%;
		width: 60%;
		padding-left:20%;
		height:100%;
	}
		.basic td, th
		{
			vertical-align: text-top !important; 
			padding-top:0.75em;
			padding-down:0.25em;
		}
		.basicForm
		{
			color: black;
			position: relative !important;
			width:70%;
			height:50%;
			top:10%;
			left:20%;
		}
		.basic strong
		{
			font-size:2em;
		}
	#trans
	{
		background-color: white;
		text-align: center;
		position: fixed;
		top: 0;
		left: 0;
		width: 100%;
		height:100%;
	}
		#trans .myform
		{
			color: black;
			position: relative;
			top: 15%;
			left:0;
		}
	.bar
	{
		background-color: white;
		//background-color: rgba(10%, 13%, 10%, 0.8);
		text-align: center;
		position: fixed;
		overflow-y:auto;
		top: 0;
		left: 0;
		width: 100%;
		height:100%;
	}
		.barForm
		{
			color: black;
			position: relative !important;
			width:70%;
			height:50%;
			top:15%;
			left:20%;
		}
		
    #nav
	{
		text-align: left;
		position: fixed;
		left: 0;
		top: 20%;
		width: 13em;
		z-index: 10000;
		cursor: default;
		display:block;
	}
	
		#nav ul
		{
			margin-bottom: 0;
		}
	
		#nav li
		{
			display: block;
			margin-left:0;
			margin-bottom: 0.5em;
			margin-top: 0.25em;
		}
		
		#nav a
		{
			position: relative;
			display: block;
			color: #808080  ;
			text-decoration: none;
			outline: 0;
		}
		
			#nav a:hover
			{
				color: black !important;
				font-size:1.05em;
			}
  </style> 
</head>