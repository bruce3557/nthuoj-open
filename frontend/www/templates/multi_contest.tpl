<h3>Current Contests</h3>
<table class="table table-striped">
  <tr>
    <th width="20%">Contest ID </th>
    <th>Contest Name</th>
	<th>Score Board</th>
    <th>Start Time</th>
    <th>End Time</th>
  </tr>

  <{section name=id loop=$contest }>
    <tr>
      <td><{$contest[id].cid}></td>
      <td>
        <a href=contest.php?cid=<{$contest[id].cid}>><{$contest[id].cname}></a>
      </td>
	  <td> 
          <a href=scoreboard.php?cid=<{$contest[id].cid}>>Score Board</a>
      </td>
	  <td><{$contest[id].start_time}></td>
      <td><{$contest[id].end_time}></td>
    </tr>
  <{/section}>
</table>
