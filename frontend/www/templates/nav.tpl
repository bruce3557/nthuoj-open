<div class="navbar navbar-inverse navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container-fluid">
      <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <a class="brand" href="./index.php"><img src="./pic/TitlebarLogo2.png" width="150"></a>
      <div class="nav-collapse collapse">
        <ul class="nav">
          <{if $is_login }>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Personal<b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="./my_submission.php">Submission</a></li>
                <li><a href="./my_statistics.php">Statistics</a></li>
                <li><a href="./profile.php">Profile</a></li>
              </ul>
            </li>
          <{/if}>

          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Contest<b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="./contest_archive.php">Archive</a></li>
              <li><a href="./multi_contest_archive.php">Problem Set</a></li>
              <li><a href="./multi_contest_archive.php">Scoreboard</a></li>
            </ul>
          </li>


          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Volume<b class="caret"></b></a>

            <ul class="dropdown-menu">

              <{section name=aa loop=$probs}>
                <li><a href="./volume.php?vol=<{$probs[aa].id}>">Volume <{$probs[aa].id}></a></li>
              <{/section}>
            </ul>

          </li>








          <{if $is_admin }>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Admins<b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="./contest_panel_main.php">Contest Panel</a></li>
              <li><a href="./problem_panel.php">Problem Panel</a></li>
              <li><a href="./user_panel.php">Users Panel</a></li>
              <li><a href="./format_transfer.php">Format Transformation</a></li>
	      <li><a href="./setUserLevel.php">Set User Level</a></li>
	      <li><a href='./findUserSubmissions.php'>Find User Submissions</a></li>
	      <!--<li><a href="./findSid.php">Find Sid</a></li>-->
	      <li><a href="./rejudge.php">Rejudge</a></li>
            </ul>
          </li>
          <{/if}>

          <li><a href="./submit.php">Submit</a></li>
          <li><a href="./status.php">Status</a></li>
          <li><a href="./user_rank.php">User Rank</a></li>
        </ul>

        <{if not $is_login }>
          <button type="submit" class="btn pull-right" onClick="window.location='./register.php'">Register</button>
          <form class="navbar-form pull-right" method="post" action="./login.php">
            <input class="span1" type="text" name="txtUserId" placeholder="Account">
            <input class="span1" type="password" name="txtPassword" placeholder="Passwd">
            <button type="submit" class="btn" name="btnLogin">Sign in</button>&nbsp;
          </form>
        <{else}>
          <button type="submit" class="btn pull-right" onClick="window.location='./logout.php'">Logout</button>
        <{/if}>
      </div>
    </div>
  </div>
</div>
