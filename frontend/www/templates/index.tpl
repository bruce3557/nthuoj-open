
<!--<div class="hero-unit">-->
  <div class="carousel slide" id="myCarousel">
    <div class="carousel-inner">
      <div class="active item">
      <div class="hero-unit">
      <img src="./pic/TitlebarLogo.png" width="500">
      <table class="table">
        <tr>
        <!--<td rowspan=2><img src="./pic/logl2.png"></td>-->
        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td>
          <!--h2>NTHU Online Judge</h2>-->
          <p>This is National Tsing Hua University Online Judge, an ACM-like online judge designed for training purposes and a platform of contest. You may utilize this site by the links on the side bar and top-right corner. Each of them will redirect you to a page with the following functions.</p>
          <p>
          <{if $is_login }>
            
          <{else}>
            <a class="btn btn-primary btn-large" href="./register.php">Register &raquo;</a>
            <a class="btn btn-primary btn-large" href="./login.php">Login &raquo;</a>
          <{/if}>
          </p>
        </td>
        </tr>
      </table>
      </div></div>

      <{if $msg != '' }>
        <div class="item">
        <div class="hero-unit">
          <h2><{ $msg }></h2>
          <hr>
          <p>&nbsp;Click the link and join the contest!!</p>
          <br><br><br><br>
        </div>
        </div>
      <{/if}>
	  

	  <div class="item"><div class="hero-unit">
        <h2>Contact US!!</h2>
        <hr>
        <p>&nbsp;If you have any problem with any question, feel free to contact us.</p>
        <br><br><br><br><br>
      </div></div>
    </div>
    <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
    <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
  </div>
<!--</div>-->

<div class="row">
<!---
  <div class="span3">
    <h3>Contact Them</h3>
    <p>If you have any problem with any question, feel free to contact them.</p>
    <dl>
      <dt>Quarterback</dt>
      <dd><p>The most talented C/C++ coder ever seen in history.(Single)</p></dd>
      <dt>搗蛋</dt>
      <dd><p>黃金單身漢，歡迎學妹洽詢</p></dd>
    </dl>
    <p></p>
  </div>
  --->
    <div class="span3">
    <h3>Contact Us</h3>
    <p>If you have any system-wide problem to report, please contact us.</p>
    <{section name=ids loop=$rs }>
		<dl>
		  <dt><{$rs[ids].id}></dt>
		  <dd><p><{$rs[ids].email}></p></dd>
		</dl>
		<p></p>
	<{/section}>
  </div>
</div>

