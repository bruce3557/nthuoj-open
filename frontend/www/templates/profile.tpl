<h3>Profile</h3>
<div>
  <form method="post" name="frmLogin" enctype="multipart/form-data">
  <table class="table">
    <tr>
      <th>User ID</th>
      <td><{ $uid }></td>
    </tr>
    <tr>
      <th>Password</th>
      <td><input name="password" type="password" size=12 maxlength=12></td>
    </tr>
    <tr>
      <th>Confirm Password</th>
      <td><input name="confirm_password" type="password" size=12 maxlength=12></td>
    </tr>
    <tr>
      <th>Email Address</th>
      <td><input name="email" type="text" size=32 maxlength=64 value="<{ $email }>" ></td>
    </tr>
    <tr>
      <th>Nickname</th>
      <td><input name="nickname" type="text" size=12 maxlength=12 value="<{ $nickname }>" ></td>
    </tr>
    <tr>
      <td colspan=2><input type="submit" name="btnSubmit" value="Update" class="btn btn-primary" style="align: right"></td>
    </tr>
  </table>
  </form>
</div>
