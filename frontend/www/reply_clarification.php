<?php
/*************************
reply_clarification.php 
This page is used to reply other's clarification.
Checks POST parameter 'submit' to trigger reply action.
Reply information is in POST 'reply' parameter.
Replyee is in GET parameter 'clid'
 * **********************/
    session_start();
    include_once("lib/base.php");
    include_once("lib/contest_lib.php");
    if( !check_login() || !check_admin() ) {
        echo "<script>alert('Please Login');window.location('./login.php');</script>";
        exit;
    }

    if( isset($_POST['submit']) ) {
        $clid = $_GET['clid'];
        $reply = SQL_injection($_POST['reply'.$clid]);
        reply_clarification($clid, $reply);
        header("Location: ./contest.php?cid=".$cid);
    }
    header("Location: ./contest.php");
?>
