<?php
/*****************
 * status_lib.php
 * This provides a status related function library.
 * ***************/
 
 /***************
 Define three kinds of status. 1. system info 2. AC 3. not AC
 Checks which kind of status the input is.
 ****************/
	function getStatus($s)
	{
		if(isSystem($s))
			return "system";
		else if(isAccepted($s))
			return "AC"; 
		else
			return "not AC";
	}
	
/****************
Get a color related to the status. Different colors are shown in status page.
***************/
	function getStatusColor($s)
	{
		$status = getStatus($s);
		if($status=="system")
			return '"blue"';
		if($status=="AC")
			return '"red"';
		if($status=="not AC")	
			return '"green"';
		return '""';
	}
	
/*****************
 Change origin status info formate to the formate to show in status page.
******************/
	function showStatus($s)
	{
		$status = getStatus($s);
		if($status=="system")
			return $s;
		else if($status=="AC")
			return "All Accepted (".$s.")";
		else	return "Not Accepted (".$s.")";
	}
/***************
Checks whether the status formate is a system information.
****************/
	function isSystem($s)
	{
		$arr = explode("/", $s);
		if( count($arr) == 2)
			return false;
		return true;
	}
/***************
Checks whether the status means AC.
***************/
	function isAccepted($s)
	{
		$arr = explode("/", $s);
		if( count($arr) ==2)
			if($arr[0] == $arr[1] and $arr[0]!='0')
				return true;
		return false;
	}
?>