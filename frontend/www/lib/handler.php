<?php
/***********************
 * handler.php
 * This provides a smarty handler class and set some path for smarty.
 * ********************/
include_once("base.php");
include_once("class/Smarty.class.php");
include_once("database_tools.php");
define('__SITE_ROOT', '.');

class Handler extends Smarty
{
    public function __construct($title="", $content="")
    {
        parent::__construct();
        $this->template_dir = __SITE_ROOT . "/templates/";
        $this->compile_dir = __SITE_ROOT . "/templates_c/";
        $this->config_dir = __SITE_ROOT . "/configs/";
        $this->cache_dir = __SITE_ROOT . "/cache/";
        $this->left_delimiter = '<{';
        $this->right_delimiter = '}>';

        if(check_login())
            $this->assign("is_login", true);
        if(check_admin())
            $this->assign("is_admin", true);
        $con = get_database_object();
        $query = "Select max(pid) from problems";
        $result = mysql_query($query) or die("GG".mysql_error());
        $row = mysql_fetch_array($result);
        $num = $row[0]/1000;
        
        $probs = array();
        for($i=0;$i<=$num;$i++)
        {
            $new = array();
            $new['id']=$i;
            array_push($probs,$new);
        }
        $this->assign("probs",$probs);
        $title = "NTHU Online Judge: ".$title;  // TITLE COULD BE RESET
        $this->assign("title", $title);
        $this->assign("content", $content);
    }
}

?>
