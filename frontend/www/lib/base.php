<?php
/*
  (Bruce Kuo, 2013/1/24)
  This is for basic libraries that are not dependent on any other file.
*/
ini_set('date.timezone','Asia/Taipei');

/***************************
This is used in old structure of code.
Latest version of 'getStatusColor' is in status_lib.php
*****************************/
function get_status_color($status)
{
    // Get the color of status
    if(stripos($status, "accepted") !== false)
        return '"red"';
    elseif(stripos($status, "wrong answer") !== false)
        return '"green"';
    elseif(stripos($status, "compile error") !== false || stripos($status, "compilation error") !== false)
        return '"blue"';
    else
        return '""';
}

/*************************
 * Gives a 'msg' and would add a '\' to symbols: ''', '"', 'NULL', '\' --> '\'', '\"', '\NULL', '\\'.
 * Therefore, 'msg' can be used as sql with those symbols.
 *************************/
function SQL_injection($msg)
{
    if( !get_magic_quotes_gpc() ) {
        if( is_array($msg) ) {
            foreach($msg as $key => $value)
                $msg[$key] = addslashes($value);
        } else {
            $msg = addslashes($msg);
        }
    }
    return $msg;
}

/*****************
 * This checks the contest name's correctness.
 * ****************/
function check_cname($name)
{
    $len = strlen($name);
    return ($len >= 1 && $len <= 64);
}

/***************
 * This checks the formate correctness of a problem's name.
 * **************/
function check_pname($name)
{
    $len = strlen($name);
    return ($len >= 1 && $len <= 64);
}

/*************
 * This checks the problem setter's formate correctness.
 * ***************/
function check_pbname($name)
{
    $len = strlen($name);
    return ($len >= 0 && $len <= 32);
}

/*******************
 * This checks the pid's formate correctness. (problem ID)
 * *******************/
function check_pid($pid)
{
    require_once("validation.php");
    $errors = array();
    $rules = array();
    $rules[] = "required,pid,Problem ID is empty.";
    $rules[] = "digits_only,pid,Invalid problem ID.";
//    $rules[] = "range=1000-9999,pid,Invalid problem ID.";

    $errors = validateFields($_POST, $rules);
    if( !empty($errors) )
        return $errors[0];
    else
        return "";
}

/******************
 * This checks the formate correctness of rid (root problem's ID).
 * *****************/
function check_rid($rid)
{
	if($rid<0)
		return false;
	return true;
}

/**************
 * This checks whether the user has logged in.
 * *************/
function check_login()
{
    return (isset($_SESSION['db_is_logged_in']) && $_SESSION['db_is_logged_in']);
}

/***********
 * This checks whether the time period is valid.( Time b should be later than time a.)
 * **********/
function check_valid_time($time_a, $time_b)
{
    return (strtotime($time_a) < strtotime($time_b));
}

/********************
 *  Check and uncompress a ror file.
 * Path of the uncompressed file is returned if successfully uncompressed. Otherwise, false is returned.
 * *********************/
function check_uncompress($path, $out_path)
{
    $ext = substr($path, strrpos($path, ".") + 1);
    $ext = strtoupper($ext);

    if($ext == "RAR") {
        if(!$rar_file = rar_open($out_path))
            return false;
        $list = rar_list($rar_file);
        if(count($list) != 1) {
            rar_close($rar_file);
            return false;
        }
        foreach($list as $file) {
            $file->extract("/tmp/");
            rar_close($rar_file);
            return "/tmp/".$file->getName();
        }
    } else {
        return $out_path;
    }
}

/*****************
 * This checks the user's level and return it.
 * *****************/
function check_user_level($now, $test_level)
{
    // Check userlevel
    return (isset($now) && $now >= $test_level);
}

/****************
 * This checks whether the user is admin/judge/coowner.
 * ***************/
function check_admin()
{
    if(isset($_SESSION['isAdmin']))
		return true;
	else if(isset($_SESSION['isJudge']))
		return true;
	else if(isset($_SESSION['isCoowner']))
		return true;
	return false;
	
}
/*************
 * This checks thether the user is admin/judge/coowner
 * ***********/
function check_coowner($lev = NULL)
{	 
	
	if(isset($_SESSION['isAdmin']))
		return true;
	else if(isset($_SESSION['isJudge']))
		return true;
	else if(isset($_SESSION['isCoowner']))
		return true;
	return false;
}
/********
 * This checks whether the user is admin
 * *********/
function check_adminis()
{

	if(isset($_SESSION['isAdmin']))
		return true;
	return false;
}

/***************
 * This checks whether user is admin/judge
 * *************/
function check_judge()
{
    
	if(isset($_SESSION['isAdmin']))
		return true;
    if(isset($_SESSION['isJudge']))
		return true;
	return false;
}

/***************
 * This checks whether the contest is freezing.
 * ***************/
function check_freeze($freeze, $st, $et)
{
    // Check whether the contest is freeze
    if( !ctype_digit($freeze) )
        return false;
    if($freeze < 0)
        return false;
    if($freeze > (strtotime($et) - strtotime($st))/60)
        return false;
    return true;
}

/*****************
 * This Convert the format between windows and linux
 * *************/
function convert_upload($path, $tmp_path)
{
    // Convert the format between windows and linux
    $dosfile = fopen($tmp_path, "r");
    $fp = fopen($path, "w");
	if($dosfile == false){
		echo "<script>console.log('dos open file fail');</script>\n";
	}
	if($fp == false){
		echo "<script>console.log('fp open file fail');</script>\n";
	}
	while( !feof($dosfile) ) {
        $line = fgets($dosfile);
        $unixfile = str_replace("\r\n", "\n", $line);
        fwrite($fp, $unixfile);
    }
    fclose($dosfile);
    fclose($fp);
    chmod($path, 0666);
	if(file_exists($path))
		echo "<script>console.log('exists');</script>\n";
	else
		echo "<script>console.log('not exists');</script>\n";
}

function set_top_message($msg)
{
    if($msg != '')
        echo '<h5 class="info-title">'.$msg.'</h5>';
}
/**********
 * This turns a period of time by second into "hr:min:sec" formate.
 * ***********/
function toperiod($period)
{
    $second = $period % 60;
    $period = $period / 60;
    $minute = $period % 60;
    $hour = $period / 60;

    return sprintf("%02d:%02d:%02d", $hour, $minute, $second);
}

/* check whether the user is higher than normal user */
function check_advusr()
{
    if(isset($_SESSION['isAdmin']))
		return true;
	else if(isset($_SESSION['isJudge']))
		return true;
	else if(isset($_SESSION['isCoowner']))
		return true;
	return false;
}

/*************
 * Provides js alert.
 * ***********/
function alert($str)
{
	echo "<script> alert($str) </script>";
}

?>
