<?php
/***************
 * problem_lib.php
 * This provides a problem related function library.
 * *************/
 
 /*********
  * Given a 'pid', the root problem 'pid' ($rootPid) is returned.
  * ********/
function getRootPid($pid)
{
	$pa = getParentPid($pid);
	if($pa==0)	return $pid; //this is root of prob tree
	$rootPid = getRootPid($pa);
	$query = "UPDATE problems SET parent_pid = $rootPid WHERE pid = $pid " ;
	mysql_query($query) or die("Query failed in getRootPid! ".mysql_error());
	return $rootPid;
}

/************
 * Given a 'pid', return the problem's parent problem's pid.
 * If there's no parent of the problem (probably wasn't set), return 0.
 * ***********/
function getParentPid($pid)
{
	if($pid==0) return 0; // default parent_pid value=0 no stop in here --> GG
	$query = "SELECT * FROM problems WHERE pid =".$pid;
	$result = mysql_query($query) or die("Query failed in getParentPid! ".mysql_error());
	$row = mysql_fetch_array($result,MYSQL_ASSOC);
	if(!$row)
		die('Problem ID does not exist');
	return $row['parent_pid'];
}
/****************
 * This checks whether a pid is a root problem pid or not.
 * **************/
function checkIsRootPid($pid)
{
	if(0==getParentPid($pid))
		return true;
	else return false;
}

/* Given pid and uid, check whether the user is the problem's setter*/
function checkProblemSetter($pid, $uid)
{
    $query = "SELECT * FROM problems WHERE pid = ".$pid;
    $result = mysql_query($query) or die("Query failed in checkProblemSetter! ".mysql_error());
	$row = mysql_fetch_array($result,MYSQL_ASSOC);
	if($row['problemsetter'] == $uid)
	    return true;
	return false;
}

?>