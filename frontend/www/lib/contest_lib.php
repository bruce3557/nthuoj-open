<?php
/***********************
 * contest_lib.php
 * This php provides functions related to contests.
 * *********************/
include_once("base.php");
include_once("database_tools.php");

/***************
 * This checks whether a contest's start time and end time has no conflict with other contest.
 * However, this is used in old system (single contest at a time).
 * This is not being used now.
 * ************/
function check_interval($st, $et, $cid)
{
    $st_stamp = strtotime($st);
    $et_stamp = strtotime($et);
    if($st_stamp > $et_stamp) return false;
    
    $con = get_database_object();

    $query = "SELECT * FROM contest WHERE '".$st."' >= start_time AND '".$st."' < end_time AND cid != ".$cid;
    $result = mysql_query($query) or die('query failed'.mysql_error());
    if(mysql_num_rows($result) != 0) {
        $row = mysql_fetch_array($result);
        return false;
    }
    
    $query = "SELECT * FROM contest WHERE '".$et."' >= start_time AND '".$et."' < end_time AND cid !=".$cid;
    $result = mysql_query($query) or die('query failed'.mysql_error());
    if(mysql_num_rows($result) != 0) {
        echo "ZZZ";
        return false;
    }

    return true;
}

/**************************
 * This get the latest contest that has been nearly or already to start.
 * ************************/
function get_latest_contests($con2 = NULL)
{
    if(!$con2) {
        $tmp = 1;
        $con2 = get_database_object();
    } else {
        $tmp = 0;
    }

    $query = "SELECT * FROM contest WHERE NOW() >= start_time AND NOW() <= end_time ORDER BY start_time DESC";
    $result = mysql_query($query) or die(mysql_error());
    if($tmp)
        mysql_close($con2);
    $arr = array();
    while($entry = mysql_fetch_assoc($result))
        array_push($arr,$entry['cid']);
    if (empty($errors)) {
        return -1;
    }
    else
        return arr;
}
function get_latest_contest($con2 = NULL)
{
    if(!$con2) {
        $tmp = 1;
        $con2 = get_database_object();
    } else {
        $tmp = 0;
    }

    $query = "SELECT * FROM contest WHERE NOW() >= start_time ORDER BY start_time DESC LIMIT 1";
    $result = mysql_query($query) or die(mysql_error());
    if($tmp)
        mysql_close($con2);

    if($entry = mysql_fetch_assoc($result))
        return $entry['cid'];
    else
        return -1;
}

/**************
 * This gives clarification considering different user level.
 * An admin sees all clarification.
 * An login user sees his/her and all general clarification.
 * An un logged in user sees general clarification.
 * SQL result is returned.
 * ************/
function get_clarification($uid, $cid)
{
    $con = get_database_object();

    if( check_admin() ) {
        $query = "SELECT * FROM clarification
                  WHERE cid=$cid
                  ORDER BY time DESC
                 ";
    } elseif( !check_login() ) {
        $query = "SELECT * FROM clarification
                  WHERE cid=$cid
                    AND uid='_general'
                    AND solved=1
                  ORDER BY time DESC
                 ";
    } else {
        $query = "SELECT * FROM clarification
                  WHERE (uid='$uid' OR (uid='_general' AND solved=1))
                    AND (cid=$cid)
                  ORDER BY time DESC";
    }
    $result = mysql_query($query) or die(mysql_error());

    mysql_close($con);
    return $result;
}

/*******************
 * This put the clarification into DB.
 * ****************/
function send_clarification($uid, $cid, $pid, $title, $msg)
{
    $con = get_database_object();
    if( check_admin() ) {
        $query = "INSERT INTO clarification (uid, cid, pid, title, msg, solved)
                  VALUES ('$uid', $cid, $pid, '$title', '$msg', 1)";
    } else {
        $query = "INSERT INTO clarification (uid, cid, pid, title, msg)
                  VALUES ('$uid', $cid, $pid, '$title', '$msg')";
    }
    mysql_query($query) or die(mysql_error());
    mysql_close($con);
}

/******************
 * This updates (reply) clarification.
 * ***************/
function reply_clarification($clid, $reply)
{
    $con = get_database_object();
    $query = "UPDATE clarification
              SET solved=1, reply='$reply' 
              WHERE clid=$clid";
    mysql_query($query) or die(mysql_error());
    mysql_close($con);
}

/****************
 * This force the page to multi_contest_archive.php
 * *************/
function go_multi_contest_page()
{
    echo "<script lanuage='javascript'>location.href='multi_contest_archive.php'</script>";
}

/**************
 * This function gets all current running contests.
 * ************/
function getRunningContest()
{
    $con = get_database_object();   
    $cur_t = time();
    $query = "SELECT cid, cname, start_time, end_time, result 
              FROM contest 
              ORDER BY start_time DESC";
    $result = mysql_query($query) or die("Query failed".mysql_error());
    
    $rs = array();
    while($row = mysql_fetch_array($result,MYSQL_ASSOC)) {
        if(time() < strtotime($row['start_time']))  continue;
        if(time() > strtotime($row['end_time']) ) continue;
        // cid == 1 is special case, a general contest
        if($row['cid'] == 1)    continue;
        array_push($rs, $row);
    }
    //$rs_time_reverse = array_reverse($rs);
    //mysql_close($con);
    return $rs;
}

/*********
given uid, return all cid that coowned by the user
*********/
function getUserCoownedContest($uid)
{
    $query = "SELECT cid
              FROM contest_coowner
              WHERE id = '$uid'";
    $result = mysql_query($query) or die("Query failed in getUserOwnedContest".mysql_error());
    $cidArr = array();
    while($row = mysql_fetch_array($result,MYSQL_ASSOC)) {
        array_push($cidArr, $row['cid']);
    }
    return $cidArr;
}
/*********
given uid, return all cid that owned by the user
*********/
function getUserOwnedContest($uid)
{
    $query = "SELECT cid
              FROM contest
              WHERE owner = '$uid'";
    $result = mysql_query($query) or die("Query failed in getUserOwnedContest".mysql_error());
    $cidArr = array();
    while($row = mysql_fetch_array($result,MYSQL_ASSOC)) {
        array_push($cidArr, $row['cid']);
    }
    return $cidArr;
}

/****
given uid and pid, check whether the user is the owner of a contest with the problem
*****/
function isContestOwnerOfTheProblem($uid, $pid)
{
    $cidArr = getUserOwnedContest($uid);
    foreach ($cidArr as $cid) {
        if( isProblemInContest($pid, $cid) )
            return true;
    }
    return false;
}

/****
given uid and pid, check whether the user is the coowner of a contest with the problem
*****/
function isContestCoownerOfTheProblem($uid, $pid)
{
    $cidArr = getUserCoownedContest($uid);
    foreach ($cidArr as $cid) {
        if( isProblemInContest($pid, $cid) )
            return true;
    }
    return false;
}

/*****
given pid and cid, check whether the problem is in the contest
****/
function isProblemInContest($pid, $cid)
{
    $query = "SELECT *
              FROM pid_cid
              WHERE pid=$pid AND cid=$cid";
    $result = mysql_query($query) or die("Query failed in isProblemInContest".mysql_error());
    
    if( $row = mysql_fetch_array($result,MYSQL_ASSOC) ) 
        return true;

    return false;
}
/***
given pid and date, check whether the date is under any contest that hold the problem
***/
function isDuringContestTime($pid, $date)
{
    $query = "SELECT cid
              FROM pid_cid
              WHERE pid=$pid";

    $result = mysql_query($query) or die("Query failed".mysql_error());
    
    while( $row = mysql_fetch_array($result,MYSQL_ASSOC) ) {
        $query2 = " SELECT start_time, end_time
                   FROM contest
                   WHERE cid = ".$row['cid'];
        $result2 = mysql_query($query2) or die("Query failed".mysql_error());
        while( $row2 = mysql_fetch_array($result2,MYSQL_ASSOC) ){
            if( strcmp($row2['start_time'], $date)<=0 && strcmp($date, $row2['end_time'])<=0  )
                return true;
        }
    }
    return false;
}

?>