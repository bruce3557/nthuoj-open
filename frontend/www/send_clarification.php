/***********************************
send_clarification.php
This renders a page for send clrification.
check GET parameter 'submit',
to decide whether to send the clarification to db.
************************************/

<?php
    session_start();
    include_once("lib/base.php");
    include_once("lib/contest_lib.php");
    if( !check_login() ) {
        echo "<script>alert('Please Login');window.location('./login.php');</script>";
        exit;
    }

    if( isset($_POST['submit']) ) {
        $uid = SQL_injection($_SESSION['uid']);
        $cid = $_POST['cid'];
        $pid = $_POST['pid'];
        $title = SQL_injection($_POST['title']);
        $msg = SQL_injection($_POST['msg']);
		$title = str_replace("<","&lt",$title);
		$title = str_replace(">","&gt",$title);
		$msg = str_replace("<","&lt",$msg);
		$msg = str_replace(">","&gt",$msg);
        if($pid == "general") {
            $uid = "_general";
            $pid = 0000;
        }

        send_clarification($uid, $cid, $pid, $title, $msg);
        header("Location: ./contest.php?cid=".$cid);
    }
    header("Location: ./contest.php");
?>
