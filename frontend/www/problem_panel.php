<?php
/****************************************
problem_panel.php
This gives a page that shows all problems, and could add new problem.
Checks GET parameter 'del' to delete a problem.
Checks POST parameter 'submit' and 'pid' to add new problem. 
*****************************************/

	session_start();
    include_once("lib/base.php");
    include_once("lib/contest_lib.php");
    include_once("lib/database_tools.php");
    include_once("lib/handler.php");
    
    if( !check_admin() )
        die("You have no judge account");
    
    $current_user=$_SESSION['uid'];
	$errors = array();
	$message = '';

   
	$con = get_database_object();
	
	if(isset($_GET['del'])) {
       

		$query = "DELETE FROM submissions WHERE pid = ".$_GET['del'];
		mysql_query($query) or die('query failed'.mysql_error());

		$query = "DELETE FROM problems WHERE pid = ".$_GET['del'];
		mysql_query($query) or die('query failed'.mysql_error());

		$query = "DELETE FROM pid_cid WHERE pid = ".$_GET['del'];
		mysql_query($query) or die('query failed'.mysql_error());

		$query = "DELETE FROM mapping WHERE pid = ".$_GET['del'];
		mysql_query($query) or die('query failed'.mysql_error());

		$query = "DELETE FROM hashcode_pid where pid =".$_GET['del'];
		mysql_query($query) or die ('query failed'.mysql_error());
		
		$path = $DB_DATA."testdata/".$_GET['del'].".in";
		if(file_exists($path))
			unlink($path);
		$path = $DB_DATA."testdata/".$_GET['del'].".out";
		if(file_exists($path))
			unlink($path);

		header('Location: problem_panel.php');		
		exit;
	}else

	if(isset($_POST['submit'])) {
		$query = "SELECT Max(pid) as oldID FROM problems";
		$result = mysql_query($query) or die('query failed'.mysql_error());
		$row = mysql_fetch_assoc($result);
		$newID= $row['oldID']+1;
		$query = "INSERT INTO problems (pid, problemsetter) VALUES (". $newID . ", '$current_user' )";
		mysql_query($query) or die('query failed'.mysql_error());
		
		$str = $newID;
		do{
			$str = md5($str);
			$query = "select hashcode from hashcode_pid where hashcode = '$str'";
			$result = mysql_query($query)or die("GG".mysql_error());
		}while(mysql_num_rows($result)>0);
		
		$query = "INSERT INTO hashcode_pid(hashcode, pid) VALUES ('$str',{$newID})
		";
		mysql_query($query) or die("GG".mysql_error());
		mysql_close($con);
		header('Location: edit_problem.php?pid='.$newID);
		exit;
	}
    
    if( check_adminis() )
        $query = "SELECT * FROM problems ORDER BY pid desc";
    else
	    $query = "SELECT * FROM problems WHERE problemsetter='$current_user' ORDER BY pid desc";
	$result = mysql_query($query) or die("Query failed".mysql_error());

    $rs = array();
	while($row = mysql_fetch_array($result, MYSQL_ASSOC))
        array_push($rs, $row);
    $tpl = new Handler("Problem Panel", "problem_panel.tpl");
    $tpl->assign("rs", $rs);
    $tpl->assign("msg", $message);
    $tpl->display("base.html");
?>
