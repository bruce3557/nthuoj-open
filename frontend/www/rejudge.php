<?php
/**************************
rejudge.php
This page gives twp rejudge options: rejudge by problem and rejudge all.
Checks POST parameter 'rejudge' as sign to rejudge.
Checks POST parameter 'pid' to rejudge a specified problem's submissions.
Checks POST parameter 'rejudge-all' as a sign to rejudge all submissions.
 * *************************/
	session_start();
	include_once("lib/base.php");
    include_once("lib/database_tools.php");
    include_once("lib/handler.php");
	if( !check_judge() )
        die("You don't have judge permission");
	$con  = get_database_object();
	$message="";
	if(isset($_POST['rejudge'])){
		if(isset($_POST['pid'])&&  ctype_digit($_POST['pid'])){
			//echo "pid=".$pid;
			$pid = $_POST['pid'];
			$query = "select * from `problems` where pid = {$pid}";
			$result = mysql_query($query) or die("query failed".mysql_error()."<br />");
			if(mysql_num_rows($result)!=0){
				$query = "START transaction";
				$result = mysql_query($query) or die("query failed".mysql_error()."<br />");
				$query = "delete from `submission_result_detail` where pid = {$pid}"; 
				$result = mysql_query($query) or die("query failed".mysql_error()."<br />");
				$query = "update `submissions` set status = 'Being Judged' where pid = {$pid}";
				$result = mysql_query($query) or die("query failed".mysql_error()."<br />");
				$query = "commit";
				$result = mysql_query($query) or die("query failed".mysql_error()."<br />");
				$message = "Rejudging Problem {$pid}";
			}else{
				$message = "No such problem";
			}
		}else{
			$message = "No such problem";
		}
		
	}
	/*else if(isset($_POST['rejudge-all'])){
		
		$query = "START transaction";
		$result = mysql_query($query) or die("query failed".mysql_error()."<br />");
		$query = "truncate `submission_result_detail`"; 
		$result = mysql_query($query) or die("query failed".mysql_error()."<br />");
		$query = "update `submissions` set status = 'Being Judged' where 1";
		$result = mysql_query($query) or die("query failed".mysql_error()."<br />");
		$query = "commit";
		$result = mysql_query($query) or die("query failed".mysql_error()."<br />");
		$message = "Start Rejudging All Problems";
	}*/
	$tpl = new Handler("Rejudge", "rejudge.tpl");
	$tpl->assign("msg",$message);
	$tpl->display("base.html");
?>
