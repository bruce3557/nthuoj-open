<?php
	session_start();
	$message = '';
    include_once("lib/base.php");
    include_once("lib/database_tools.php");
    include_once("lib/handler.php");


	$con = get_database_object();

	$sid = $_GET['sid'];
	if(!isset($sid))
		die('sid does not exist');
	if(!is_numeric($sid))
		die('sid does not exist');

    if( !check_login() )
		die('sid does not exist');
    else if( !check_admin() ) {
		$query = "SELECT err_msg 
                  FROM submissions 
                  WHERE status='Compile Error' 
                    AND uid='".$_SESSION['uid']."' 
                    AND sid = ".$sid;
	} else {
		$query = "SELECT err_msg 
                  FROM submissions 
                  WHERE status='Compile Error' 
                    AND sid=".$sid;
    }

    $tpl = new Handler("NTHU Online Judge: Error Message of Submission ".$sid, "err_msg.tpl");
	$result = mysql_query($query) or die(mysql_error());
	$row = mysql_fetch_assoc($result);
	if(!$row) {
		die($query.'   sid does not exist / please contact staff and judge');
	} else {
		$err =  htmlspecialchars_decode($row["err_msg"],ENT_QUOTES);
		$err = str_replace("\n","</br>",$err);
        $tpl->assign("err", $err);
	}

    $tpl->display("base.html");
?>
