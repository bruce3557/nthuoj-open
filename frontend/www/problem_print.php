﻿<?php
/******************
problem_print.php
This page shows how it would look like if you would like to print a problem.
GET parameter 'pid' is checked to identify which problem to show.
*******************/
	session_start();
	include_once("lib/base.php");
    include_once("lib/contest_lib.php");
	include_once("lib/problem_lib.php");
    include_once("lib/database_tools.php");
    include_once("lib/handler.php");


	$key = array();
	$title = array();
	$key[0] = 'description';
	$key[1] = 'input';
	$key[2] = 'output';
	$key[3] = 'sample_input';
	$key[4] = 'sample_output';
	$title[0] = 'Problem Description';
	$title[1] = 'Input';
	$title[2] = 'Output';
	$title[3] = 'Sample Input';
	$title[4] = 'Sample Output';
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
</head>
<body>
<div id="content">
<?php
	sleep(10);
	$con = get_database_object();
	if(!$con)
		die('Could not connect: '. mysql_error());
	mysql_select_db("nthuoj", $con);
	$query = "SELECT * FROM problems INNER JOIN pid_cid ON problems.pid=pid_cid.pid INNER JOIN contest ON pid_cid.cid = contest.cid WHERE (contest.cid = 1 OR NOW() >= contest.start_time) AND problems.pid=".$_GET['pid'];
	
	$result = mysql_query($query) or die(mysql_error().$query);
	if(mysql_num_rows($result) == 0)
	{
		if(!isset($_SESSION['db_is_logged_in']) || !$_SESSION['db_is_logged_in'])
		{
			die('Problem ID does not exist here');
		//	header("HTTP/1.1 403 Forbidden");	
		//	die("Forbidden");
		}
	
	}
	

	if(!isset($_GET["pid"]))
		die('Problem ID does not exist');
	$pid = $_GET["pid"];
	if(!is_numeric($pid))
		die('Problem ID does not exist');
	$query = "SELECT * FROM problems WHERE pid =".$pid;
	$result = mysql_query($query) or die("Query failed".mysql_error());
	$row = mysql_fetch_array($result,MYSQL_ASSOC);
	if(!$row)
		die('Problem ID does not exist');
	
	/*not root problem --> get info from root prob*/
	$row_root =array();
	if(!checkIsRootPid($pid)){ 
		$parent_pid = getRootPid($pid);
		if(!is_numeric($parent_pid))
			die('Parent PID does not exist');
		$query = "SELECT * FROM problems WHERE pid = ".$parent_pid;
		$result = mysql_query($query) or die("Query failed :".mysql_error().$query);
		$row_root = mysql_fetch_array($result,MYSQL_ASSOC);
		$pname = $row['pname']? $row['pname'] : $row_root['pname'];
	}
	else $parent_pid = "root";
	
	
	echo "<h2>";
	echo $pid;
	echo " - ";
	echo htmlspecialchars_decode($row["pname"],ENT_QUOTES);

	echo "</h2>";

	echo "Time limit: ".$row['time_limit']."s<br/>";
	echo "Memory limit: ".$row['memory_limit']."MB<br/>";
    if ($row["special_judge"] != "")
        echo "Judge with Validator<br/>";
	
	for($i = 0; $i <= 4; $i++)
	{
		if($row[$key[$i]])
		{
			echo "<div>";
			echo "<h3>";
			echo $title[$i];
			echo "</h3>";
			echo htmlspecialchars_decode($row[$key[$i]],ENT_QUOTES);
			echo "</div>";
		}
		else if((isset($key[$i]))&&isset($row_root[$key[$i]]))
		{
			echo "<div>";
			echo "<h3>";
			echo $title[$i];
			echo "</h3>";
			echo htmlspecialchars_decode($row_root[$key[$i]],ENT_QUOTES);
			echo "</div>";
		}
	}
	echo '<hr noshade size = "1px" color = #606070>';
	if($row['problemsetter'] && $row['anonymous']=='')
	{
		echo "<h5>Problemsetter: ".htmlspecialchars_decode($row['problemsetter'])."</h5>";
	}
	mysql_close($con);
?>



</div><!-- end of inside -->
</body>
</html>

