<?php
/**********************************************
submission_detail.php
This Renders a page that shows the submitted code.
Checks the GET parameter 'sid' to show a certain submission's code.
**********************************************/


    session_start();
    include_once("lib/base.php");
    include_once("lib/contest_lib.php");
    include_once("lib/problem_lib.php");
    include_once("lib/database_tools.php");
    include_once("lib/handler.php");
    include_once("lib/user_lib.php");

    function broken_page($err_msg = "")
    {
        $tpl = new Handler("Broken Page", "broken_page.tpl");
        $tpl->assign("displayed_msg", $err_msg);
        $tpl->display("base.html");
        exit(0);
    }

    $con = get_database_object();
    $running_contest = getRunningContest();

    if (!check_login())
      $current_user = "";
    else
      $current_user = $_SESSION["uid"];
    
    // if not logged in
    if (!check_login())
        broken_page("You are not authorized to view this source.\n");

    // if the contest is running, only the judges are able to view sources
    if (!check_admin() && count($running_contest)>0 )
        broken_page("Submission details are hidden during the contest.\n");

    if (!isset($_GET["sid"]))
        broken_page("Bad request.\n");

    $sid = $_GET["sid"];
    if (!ctype_digit($sid))
        broken_page("Bad request.\n");

    // query to get submission info
    $query = "SELECT sid, date, uid, submissions.pid, problems.pname, status, cpu, source
                FROM submissions
                INNER JOIN problems ON problems.pid = submissions.pid
                INNER JOIN users ON users.id = submissions.uid
                WHERE sid = $sid
                ";

    ($result = mysql_query($query, $con)) or die("Query failed.");
    $row = mysql_fetch_array($result);

    if (!$row)
        broken_page("The user is not found or the sid is wrong\n");

    // to see others' code if is not adminLevel
    if( $current_user != $row['uid'] && !check_adminis() ){ 
        // make sure only problem related with the user so that the code can be seen
        // only owner/coowner of a contest with the problem or the problem owner can see
        // however, owner/coowner of the contest can only see code during contest time
        if  ( ! (  ( isContestOwnerOfTheProblem($current_user, $row['pid']) 
                       || isContestCoownerOfTheProblem( $current_user, $row['pid'] ) )
                && isDuringContestTime($row['pid'], $row['date']) 
                )
              && !checkProblemSetter($row['pid'], $current_user)
            ){   
                broken_page("You are not authorized to view this code!!\n");
            }
    }

	$full_path = $DB_DATA."source/$sid.".$row['source'];
	if (!file_exists($full_path)){
		broken_page("A Source not found.\n$sid.".$row['source']);
	}else if(!is_file($full_path)){
			broken_page("B Source not found.\n$sid.".$row['source']);
	}

    $submission = $row;
    $submission["color"] = get_status_color($submission["status"]);
    $tpl = new Handler("Submission Details", "submission_details.tpl");
    $code = htmlspecialchars(file_get_contents($full_path));
    $tpl->assign("code", $code);
    $tpl->assign("submission", $submission);
    $tpl->display("base.html");

    exit(0);
?>

