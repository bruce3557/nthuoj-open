<?php
/*********************************************
submit.php
This renders a page to sumit codes.
This checks POST parameter 'sm' as a signal to show there's submission.
And POST parameter 'pid' to assign the problem.
inside = 0 means error
inside = 1 means submit through file update
inside = 2 means submit through text-area
*********************************************/

    session_start();

    include_once("lib/base.php");
    include_once("lib/contest_lib.php");
    include_once("lib/database_tools.php");
    include_once("lib/handler.php");
    include_once("validation.php");

    $message = '';
    $cip = '';
    $cip = "/*".$_SERVER['REMOTE_ADDR']."*/";
    $errors = array();
    if(!check_login()) {
        header("Location: login.php");
        exit;
    }
	
	$pid = "";
	$timelock="";
    $current_cid = get_latest_contest();
    $uid = $_SESSION['uid'];
    $cip .= "/*".$uid."*/";
	
    $inside=0;
    
    $con = get_database_object();
    
    if( isset($_POST["sm"]) || (isset($_POST["pid"]) && $_POST["pid"]) ) {
        $rules = array();
        $rules[] = "required,pid,Problem ID is empty.";
        $rules[] = "digits_only,pid,Invalid problem ID.";

        $errors = validateFields($_POST, $rules);
        if(!isset($_POST["lang"]) || !$_POST["lang"])
            $errors[] = "Please choose one of the language.";

        if( empty($errors) ) {
                if(!isset($_POST['pid'])){
                    $message = "No problem ID.";
                    echo "<script>document.location.href='./submit.php';</script>";
                    exit;
                }
                if( check_coowner() ) {
                    $query = "SELECT problems.pid 
                              FROM problems 
                              WHERE problems.pid=".$_POST['pid'];
                } else {
                    $query = "SELECT problems.pid 
                              FROM problems where visible = 'checked' and pid = {$_POST['pid']}";
                                                           ;
                }

                $result = mysql_query($query) or die('Query failed'.mysql_error());
                if( mysql_num_rows($result) >= 1 ) {
                    
                    if( $_FILES["upfile"]["name"] ) {
                        if( $_FILES["upfile"]["error"] > 0 ){
                            $message = "File uploading error.";
                        }
                        if($_FILES["upfile"]["size"] > 102400){
                            $message = "File too large. (limit: 100KB)";
                        }else{
                            $inside = 1;
                        }
                    } elseif($_POST["code"]) {
						/* revised by Bass */
						if ( get_magic_quotes_gpc() )	// magic_quotes may be different, depending on setting of php
							$source = stripslashes(trim($_POST['code']));
						else
							$source = trim($_POST['code']);
						/* end of revise */
						
                        if(strlen($source) > 102400)
                            $message = "Code size is too large. (limit: 100KB)";
                        else{
                            $inside = 2;
                        }
                    } else {
                        $message = "Source code not selected.";
                    }
                } else {
                    $message = "Invalid problem ID.";
                    echo "<script>document.location.href='./submit.php';</script>";
                    exit;
                }
            
        }else{
            $message = $errors[0];
        }
        
        if($inside!=0) {
            if( !$_POST["pid"] )    die('No problem ID');
            $pid = $_POST["pid"];
            $uid = $_SESSION["uid"];
            $lang = $_POST["lang"];

            $qry = "SELECT problems.pid FROM problems
                    WHERE problems.pid=".$pid;
            $rs = mysql_query($qry) or die('Error : No Such Problem');
            $tmp = mysql_fetch_row($rs);
            $gid = $tmp[0];
            
            $tmp_file = "/tmp/tmp.$lang";

            if($inside == 2) {
                if( !($fp=fopen($tmp_file, 'w')) )  die("Cannot open file");
                fwrite($fp, $source."\n");
                fclose($fp);
            } else {
                move_uploaded_file($_FILES["upfile"]["tmp_name"], $tmp_file) or die('move error');
            }

            $fp = fopen($tmp_file, 'a') or die("Cannot open file");
            fwrite($fp, "\n".$cip."\n");
            fclose($fp);

            $ssid = -1; //This line is added by ATH.
  

                $query = "INSERT INTO submissions (uid, pid, date, status, source) 
                          VALUES ('$uid', $pid, NOW(), 'Being Judged', '$lang')";
                mysql_query($query) or die('Error, insert query failed'.mysql_error());
                $sid = mysql_insert_id($con);

                /* revised by ATH */
                if( $ssid == -1 )   $ssid = $sid;

                $Qcmd = "UPDATE submissions SET SSID=$ssid
                         WHERE sid=$sid";
                mysql_query($Qcmd) or die('Error, update ssid error.');
                /* end of revise */

                $filename = ($sid).'.'."$lang";
                $filepath = $DB_DATA."source/$filename";
                if( file_exists($filepath) )
                    die('File already exists');
                if(!copy($tmp_file, $filepath)){
					$errorss = error_get_last();
					die('copy error'.$errors['type'].$errors['message']);
				}
				/* revised by Bass, change mode 660 to 664 for read */
                chmod($filepath, 0664) or die('chmod error');
				/* end of revise */
            
            $message = "Submit successful. Submission ID is $ssid";
            unlink($tmp_file);
        }
        mysql_close($con);
    }
    if( isset($_GET['pid']) && is_numeric($_GET['pid']) )
        $pid = $_GET['pid'];

    $tpl = new Handler("Submit Problem", "submit.tpl");
    $tpl->assign("msg", $message);
    $tpl->assign("pid", $pid);
    $tpl->assign("timelock",$timelock);
    $tpl->display('base.html');
?>
