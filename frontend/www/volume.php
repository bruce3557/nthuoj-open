<?php
/*************************
 * volume.php
 * This shows all problems start with some specified start number.
 * GET parameter 'vol' is used as the identifiew of the start number.
 * ***********************/
	session_start();

    include_once("lib/base.php");
    include_once("lib/database_tools.php");
    include_once("lib/handler.php");
	
  $vol = $_GET["vol"];
    if(!is_numeric($vol)||$vol<0){
	 header("Location:index.php");
  }
    
    $tpl = new Handler("Volume ".$vol, "volume.tpl");
    $tpl->assign("vol", $vol);
    $set= array(); 
    $con = get_database_object();
    if(check_login()){
      //echo $_SESSION['uid'];
      $query = "SELECT distinct (submission_result_detail.pid) from submission_result_detail INNER JOIN submissions on submission_result_detail.sid = submissions.sid where submissions.uid = '{$_SESSION['uid']}' and submission_result_detail.verdict = 'Accepted'";   
      //echo $query;
      $result = mysql_query($query) or die("Query Failed".mysql_error());
      while($row=mysql_fetch_array($result,MYSQL_ASSOC)){
        $set[$row['pid']] = 1;
      //  echo $row['pid'];
      }
    }

	$query = "SELECT DISTINCT(problems.pid), pname
              FROM problems 
              WHERE problems.pid>=".($vol*1000)." 
                AND problems.pid<".(($vol+1)*1000)." 
              AND problems.visible = 'checked' ORDER BY problems.pid  ";
	$result = mysql_query($query) or die("Query failed".mysql_error());

  $rs = array();

	while($row = mysql_fetch_array($result, MYSQL_ASSOC)){
        
        if(isset($set[$row['pid']])){
         //   echo $row['pid'];
            $row['accepted'] =true;
        }
        array_push($rs, $row);
  }
  $query = "select max(pid) from problems where 1";
  $result  = mysql_query(($query)) or die("GG".mysql_error());
  $row = mysql_fetch_array($result);
  $max_vol=  floor($row[0]/1000);
	mysql_close($con);
    $tpl->assign("rs", $rs);
    $tpl->assign("max_vol",$max_vol);
    $tpl->display("base.html");
?>

