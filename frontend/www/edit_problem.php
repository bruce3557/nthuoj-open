<?php
/*************************
 * edit_problem.php
 * This renders a page for editting a problem.
Checks GET parameter 'pid' to recognize which problem to update.
Checks POST parameter 'hasSubmit' as a sign to update problem.
All update information is in other POST parameter.
Checks POST parameter: parent_pid, pname, problemsetter, tid, anonymous.
If one of those parameter exists, problem would be update.
Checks FILE parameter: judge_program, jin to update file.
 * *************************/
	session_start();
    include_once("lib/base.php");
    include_once("lib/contest_lib.php");
	include_once("lib/problem_lib.php");
    include_once("lib/database_tools.php");
    include_once("lib/handler.php");
    include_once("fckeditor/fckeditor.php");
    
	
	if( !check_coowner() )
        die("You have no judge permission");

    $tpl = new Handler("Edit Problem", "edit_problem.tpl");

	$message = '';
	$current_user=$_SESSION['uid'];
    $con = get_database_object();

	if(!isset($_GET["pid"]))
		die('Problem ID does not exist~');

    $pid = $_GET['pid'];
    if(!ctype_digit($pid))
		die('Problem ID does not exist!');

	$query = "SELECT pid FROM problems WHERE pid =".$pid;
	$result = mysql_query($query) or die("Query failed 12".mysql_error());
	if(mysql_num_rows($result)==0)
		die('Problem ID does not exist');
		
    if( !check_adminis() && !checkProblemSetter($_GET["pid"], $current_user) )
        die("You must be the problemsetter of the problem or admin to modify the problem.");    
    $tpl->assign("pid", $pid);
	
	$sBasePath = './fckeditor/';
	$editor = array();
	$key = array();
	$title = array();
	$key[0] = 'description';
	$key[1] = 'input';
	$key[2] = 'output';
	$key[3] = 'sample_input';
	$key[4] = 'sample_output';

	$title[0] = 'Problem Description';
	$title[1] = 'Input';
	$title[2] = 'Output';
	$title[3] = 'Sample Input';
	$title[4] = 'Sample Output';
	$page_link = '';
	$tid = '';
	
	// for update ('save' pressed  in edit_problem page)
	if(isset($_POST['hasSubmit'])) {
		$page_link = $_POST['page_link'];
		$query = "UPDATE problems SET ";
		for($i = 0; $i <= 4; $i++) {
			if($i == 0 && $page_link != '' && $_POST[$key[$i]] == '') {
				if( get_magic_quotes_gpc() )
					$postedValue = htmlspecialchars( stripslashes(file_get_contents($page_link)), ENT_QUOTES);
				else
					$postedValue = htmlspecialchars( file_get_contents($page_link), ENT_QUOTES);
			} else {
				if( get_magic_quotes_gpc() )
					$postedValue = htmlspecialchars( stripslashes( $_POST[$key[$i] ] ),ENT_QUOTES );
				else
					$postedValue = htmlspecialchars( $_POST[$key[$i]], ENT_QUOTES ) ;
			}
			if($i!=0)
				$query = $query.",";
			$query = $query.$key[$i]."='".$postedValue."'";
		}
		$query = $query." WHERE pid = ".$pid;
		mysql_query($query) or die("Query failed 1".mysql_error());

        $query = "SELECT * FROM problems WHERE pid=".$pid;
        $result = mysql_query($query) or die("Query failed 2".mysql_error());
        $row = mysql_fetch_assoc($result);

		$message = 'Problem updated.';
		
		if(!ctype_digit($_POST['parent_pid']) ){
			$message = $message." Invalid parent pid.";
		}
		else {
			$root_pid = getRootPid($_POST['parent_pid']);
			$query = "UPDATE problems SET parent_pid= ".$root_pid." 
                      WHERE pid = ".$pid;
			mysql_query($query) or die("Query failed 3".mysql_error());
			$query = "SELECT * FROM problems WHERE pid = ".$root_pid;
			$result = mysql_query($query) or die("Query failed :".mysql_error().$query);
			$row = mysql_fetch_array($result,MYSQL_ASSOC);
		}
		
		
		if(!$_POST['pname'] && !checkIsRootPid($pid) ){
			/* if no pname, get it from root problem*/
			$query = "UPDATE problems SET pname='".htmlspecialchars($row['pname'],ENT_QUOTES)."' 
                      WHERE pid = ".$pid;
			mysql_query($query) or die("Query failed 4".mysql_error());
		}
		else if(!check_pname($_POST['pname']))
			$message = $message." Invalid problem name.";
		else {
			$query = "UPDATE problems SET pname='".htmlspecialchars($_POST['pname'],ENT_QUOTES)."' 
                      WHERE pid = ".$pid;
			mysql_query($query) or die("Query failed 5".mysql_error());
		}
		
		if(!check_pbname($_POST['problemsetter']))
			$message = $message." Invalid problemsetter name.";
		else if( check_adminis() ){
			$query = "UPDATE problems SET problemsetter='".htmlspecialchars($_POST['problemsetter'],ENT_QUOTES)."' 
                      WHERE pid = ".$pid;
			mysql_query($query) or die("Query failed 6".mysql_error());
		}
		else if(!checkProblemSetter($_GET["pid"], $_POST['problemsetter']))
		    $message .= "You cannot change the problem setter!";
		
		if( isset($_POST['tid']) ){
			$tid = $_POST['tid'];
			$query = "DELETE FROM mapping WHERE pid = ".$pid;
			mysql_query($query) or die('query failed'.mysql_error());
			if($_POST['tid']==0){
				$query = "UPDATE problems SET tid = ".$_POST['tid'].
						 " WHERE pid = ".$pid	;
				mysql_query($query) or die('query failed: '.mysql_error().$query);
			}
			else if( $_POST['rid'] ){
				$query = "UPDATE problems SET tid = ".$_POST['tid'].
						 " WHERE pid = ".$pid	;
				mysql_query($query) or die('query failed: '.mysql_error().$query);
				$query = "INSERT INTO mapping (pid, realid)  
						VALUES (".$pid.",'".$_POST['rid']."')";
				mysql_query($query) or die('query failed: '.mysql_error().$query);			
			}
			else
				$message = $message." Invalid TID or Real Id.";
		}
	
		if(isset($_POST['anonymous'])){
			$query = "UPDATE problems SET anonymous='".htmlspecialchars($_POST['anonymous'])."' 
					  WHERE pid = ".$pid;
			mysql_query($query) or die("Query failed 7".mysql_error());
		}else{
			$query = "UPDATE problems SET anonymous='unchecked' 
					  WHERE pid = ".$pid;
			mysql_query($query) or die("Query failed 7".mysql_error());

		}
		if(isset($_POST['visible'])){
			$query = "UPDATE problems SET visible='".htmlspecialchars($_POST['visible'])."' 
					  WHERE pid = ".$pid;
			mysql_query($query) or die("Query failed 7".mysql_error());
		}else{
			$query = "UPDATE problems SET visible='unchecked' 
					  WHERE pid = ".$pid;
			mysql_query($query) or die("Query failed 7".mysql_error());
		}
		$query = "UPDATE problems SET special_judge='".$_POST['special_judge']."' 
                  WHERE pid = ".$pid;
		mysql_query($query) or die("Query failed 8".mysql_error());
		
		if($_FILES["judge_program"]['name']) {
			if ($_FILES["judge_program"]["error"] > 0)
				$message = $message."Judge program uploading error.";
			elseif ($_FILES["judge_program"]["size"] > 10240)
				$message = $message."Judge program too large. (limit: 10KB)";
			else {
				if($out_path = check_uncompress($_FILES["judge_program"]["name"],$_FILES["judge_program"]["tmp_name"])) {
					$query = "SELECT special_judge FROM problems WHERE pid = ".$pid;
					$result = mysql_query($query) or die("Query failed 9".mysql_error());
					$row = mysql_fetch_assoc($result);
					if(!$row)
						die("orz");
					$path = $DB_DATA."speJudge/".$pid.".".$row['special_judge'];
					convert_upload($path,$out_path);
					if(file_exists($out_path))
						unlink($out_path);
				} else {
					$message = $message."The compressed file must contain exactly one judge program file.";
                }
			}
		}

	}
	
    $query = "SELECT * FROM problems WHERE pid =".$pid;
	$result = mysql_query($query) or die("Query failed 10".mysql_error());
	$row = mysql_fetch_array($result, MYSQL_ASSOC); 
    $query = "SELECT * FROM mapping WHERE pid =".$pid;
    $realid ="";
	$tid = $row['tid'];
    if( $tid!='0' ) {
	    $result = mysql_query($query) or die("Query failed 11".mysql_error());
        $row2 = mysql_fetch_array($result);
        $realid = $row2['realid'];
    }
	$tpl->assign("tid", $tid);
    $tpl->assign("realid", $realid);
    $tpl->assign("pname", $row['pname']);
	$tpl->assign("parent_pid", $row['parent_pid']);
    $tpl->assign("problemsetter", $row['problemsetter']);
    $tpl->assign("anonymous", $row['anonymous']);
    $tpl->assign("visible", $row['visible']);
    $tpl->assign("page_link", $page_link);

	
    if($row['special_judge']!="") {
		if($row['special_judge'] == "c")
			$ftype = "c";
		else if($row['special_judge'] == "cpp")
			$ftype = "cpp";
		else
			die("wrong");

		$path = $DB_DATA."speJudge/".$pid.".".$ftype;
		if(file_exists($path) )
			$jdg_link = '<a target = "_blank" href="testdata.php?pid='.$pid.'.'.$ftype.'">'.$pid.'.'.$ftype.'</a>';
		else
			$jdg_link = 'Judge program does not exist.';
	} else {
		$jdg_link = 'Judge program does not exist.';
    }
	
    $path = $DB_DATA."speJudge/".$pid.".jin";
	if(file_exists($path) )
		$jdg_input = '<a target="_blank" href="testdata.php?pid='.$pid.'.jin">'.$pid.'.jin</a>';
	else
		$jdg_input = 'Judge input file does not exist.';

    $tpl->assign("jdg", $row['special_judge']);
    $tpl->assign("jdg_link", $jdg_link);
    $tpl->assign("jdg_input", $jdg_input);
    
    $rs = array();
	for($i = 0; $i <= 4; $i++) {
		$editor[$i] = new FCKeditor($key[$i]);
		$editor[$i]->Config["CustomConfigurationsPath"] = "/fckeditor/myconfig.js";
		$editor[$i]->Width = "99%";
		$editor[$i]->BasePath = $sBasePath;
		$editor[$i]->Value = htmlspecialchars_decode($row[$key[$i]], ENT_QUOTES);
        $row['title'] = $title[$i];
		$row['content'] = $editor[$i]->CreateHtml();
        array_push($rs, $row);
	}
    mysql_close($con);

    $tpl->assign("msg", $message);
    $tpl->assign("rs", $rs);
    $tpl->display("base.html");
?>
