<?php
/**************************************************
edit_testcase.php
This renders a page to edit testcase.
Checks GET parameter 'pid' to identify which problem's testcase to edit.
Checks FILE parameter 'inputFile' to add new testcase file.
The testcase ID would be set automatically.
***************************************************/


    session_start();
	function write_log($str){
		echo "<script>console.log('$str');</script>";
	}
    include_once("lib/base.php");
    include_once("lib/contest_lib.php");
    include_once("lib/database_tools.php");
    include_once("lib/handler.php");
    include_once("validation.php");
	if( !check_admin() )
        die("You have no judge permission");
	 if(!isset($_GET["pid"]))
		die('Problem ID does not exist');
	
    $pid = $_GET['pid'];
    if(!ctype_digit($pid))
		die('Problem ID does not exist');
	$con = get_database_object();
	$query = "SELECT pid FROM problems WHERE pid =".$pid;
	$result = mysql_query($query) or die("Query failed".mysql_error());
	if(mysql_num_rows($result)==0)
		die('Problem ID does not exist');
	$message = 'Succeed';
	//echo "<script>console.log('hello');</script>\n";
	
	if((isset($_FILES['inputFile']['name']))&&(($_FILES['inputFile']['name']))){
			if( $_FILES["inputFile"]["error"] > 0 )
                            $message .= "inputFile uploading error.".$_FILES["inputFile"]["error"] ;
            if($_FILES["inputFile"]["size"] > 40*1024*1024)
                            $message .= "inputFile too large. (limit: 40MB)";
			if(($_FILES['outputFile']['name'])){
				if( $_FILES["outputFile"]["error"] > 0 )
                           $message .= "outputFile uploading error.". $_FILES["outputFile"]["error"];
				if($_FILES["outputFile"]["size"] > 40*1024*1024)
                            $message .= "outputFile too large. (limit: 40MB)";
				if(isset($_POST['timeLimit'])){
					$timeLimit = $_POST['timeLimit'];
					if(isset($_POST['memoryLimit'])){
						$memoryLimit = $_POST['memoryLimit'];
						if(strlen($message)==7){
							echo "<script>console.log('Update Success');</script>\n";
							write_log('Update Success');
							$qry = "INSERT into testcases SET pid = $pid, timeLimit = $timeLimit, memoryLimit = $memoryLimit";
							write_log($qry);
							$result = mysql_query($qry) or die ('Query Failed'.mysql_error());
							$tid = mysql_insert_id();
							write_log("$tid");
							if($out_input_path = check_uncompress($_FILES["inputFile"]["name"],$_FILES["inputFile"]["tmp_name"])) {
								if($out_output_path = check_uncompress($_FILES["outputFile"]["name"],$_FILES["outputFile"]["tmp_name"])) {
									
									$input_path =$DB_DATA."testdata/".$tid.".in";
									$output_path = $DB_DATA."testdata/".$tid.".out";
									convert_upload($input_path,$out_input_path);
									convert_upload($output_path,$out_output_path);
									//echo "<script>console.log('finish upload');</script>\n";
									if(file_exists($out_output_path))
										unlink($out_output_path);
									if(file_exists($out_input_path))
										unlink($out_input_path);
								}else{
									$message = $message."The compressed Output file must contain exactly one Output file.";
								}
							} else {
								if($out_path = check_uncompress($_FILES["outputFile"]["name"],$_FILES["outputFile"]["tmp_name"])) {
									$message = $message."The compressed Input file must contain exactly one Input file.";
								}else{
									$message = $message."Both compressed Input file must contain exactly one file.";
								}
							}
						}else{
							$lennn = strlen($message);
							//echo "<script>console.log('Somthing Happened $message $lennn');</script>\n";
						}
					}else{
						write_log('Empty Memory Limit');
						//echo "<script>console.log('Empty Memory Limit');</script>\n";
						
					}
				}else{
					write_log('Empty Time Limit');
					//echo "<script>console.log('Empty Time Limit');</script>\n";
				}
			}else{
				write_log('Empty Output File');
				//echo "<script>console.log('Empty Output File');</script>\n";
			}
	}else{
		if(isset($_FILES['inputFile']))
			write_log('Empty Input FileSSSS'.$_FILES['inputFile']['error']);
		else
			write_log('No $_FILES[inputFIle] error!');
		//echo "<script>console.log('Empty Input FileSSS');</script>\n";
	}
    
    $errors = array();
	$rs = array();
	$all = array();
	$query =  "SELECT * from testcases where pid = ".$pid;
	$res = mysql_query($query) or die("Query failed".mysql_error());
    while($row = mysql_fetch_array($res, MYSQL_ASSOC)){
		$all[$row['tid']]['timeLimit']=$row['timeLimit'];
		$all[$row['tid']]['memoryLimit']=$row['memoryLimit'];
		$all[$row['tid']]['testcaseId']=$row['tid'];
	}
	foreach($all as $key => $value){
		$row = array();
		$row['testcaseId'] = $all[$key]['testcaseId'];
		$row['memoryLimit'] = $all[$key]['memoryLimit'];
		$row['timeLimit'] = $all[$key]['timeLimit'];
		array_push($rs, $row);
	}
    $tpl = new Handler("Edit Testcase", "edit_testcase.tpl");
    $tpl->assign("rs",$rs);
	$tpl->assign("pid",$pid);
	$tpl->assign("msg",$message);
    $tpl->display('base2.html');
?>
	