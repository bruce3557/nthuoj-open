<?php
/*************************************************************
index.php
This gives the index page.
No parameter is required.
*************************************************************/

	session_start();
	include_once("lib/base.php");
	include_once("lib/contest_lib.php");
	include_once("lib/database_tools.php");
	include_once("lib/handler.php");
	$con = get_database_object();
	$tpl = new Handler("Index", "index.tpl");
	$rs = getRunningContest();
	$message ="";
	if(count($rs)>0){
		$message = "The following contests are running:<br />";
		for($i=0;$i<count($rs);$i++){
			$tmp="<a href=contest.php?cid=" . $rs[$i]['cid'] .">". $rs[$i]['cname'] . "</a>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" ;
			$message.=$tmp."<br />";
		}
	}
	$query = "SELECT start_time, cname FROM contest WHERE start_time > NOW()";
	
	$result = mysql_query($query) or die("Query failed who".mysql_error());
	while($row = mysql_fetch_assoc($result)){
		if($row && (strtotime($row['start_time'])-time()>0) &&(strtotime($row['start_time'])-time() < 48 * 60 * 60) ) {
			$message .= "Contest '".$row['cname']."' will be started in ".
			toperiod(strtotime($row['start_time']) - time())."<br />";
		}
	}

	$query = "SELECT id FROM admin";
	$result = mysql_query($query) or die("Query failed hello".$query.mysql_error());
	$rs = array();
	while($row = mysql_fetch_assoc($result)){
			$query2 = "SELECT id,email FROM users WHERE id LIKE '{$row['id']}' ";
			$result2 = mysql_query($query2) or die("Query failed HI".mysql_error());
			$row2 = mysql_fetch_assoc($result2);
			array_push($rs,$row2);
	}
	$tpl->assign("msg",$message);
	$tpl->assign("rs",$rs);
    $tpl->display('base.html');
?>
