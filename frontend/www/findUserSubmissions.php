<?php
/*********************************************************************
findUserSubmissions.php
This page checks POST parameter 'uid' and would go to the status page with GET parameter of the uid.
The status page would show all submiisions of the user.
**********************************************************************/

  session_start();
  require_once("lib/base.php");
  require_once("lib/contest_lib.php");
	include_once("lib/user_lib.php");
  require_once("lib/database_tools.php");
  require_once("lib/handler.php");
  $message = "";
  if( !check_admin() )
    die("You don't have judge permission");
  
  $tpl = new Handler("Finding User Submission", "status.tpl");
  $con = get_database_object();
  $rs = array();
  if( isset($_POST['uid'])) {
    if( !isUserExist($_POST['uid']) )    $message = "The User You Find Doesn't Exist!!" ;
    else
        echo "<script lanuage='javascript'>location.href='status.php?uid=".$_POST['uid']."'</script>";
  }
  

  $tpl->assign("rs", $rs);
  $tpl->assign("findUser", 1);
  $tpl->assign("msg", $message);
  mysql_close($con);

  $tpl->display("base.html");
?>
