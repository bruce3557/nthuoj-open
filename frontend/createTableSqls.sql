create database if not exists nthuoj;
use nthuoj;

--
-- Database: `nthuoj`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` varchar(16) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `clarification`
--

CREATE TABLE IF NOT EXISTS `clarification` (
  `clid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` varchar(12) NOT NULL,
  `pid` int(11) unsigned NOT NULL,
  `cid` int(11) unsigned NOT NULL,
  `msg` longtext CHARACTER SET ucs2 NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `solved` tinyint(1) NOT NULL DEFAULT '0',
  `reply` text,
  `title` varchar(100) NOT NULL,
  PRIMARY KEY (`clid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='contest clarification' 
AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `contest`
--

CREATE TABLE IF NOT EXISTS `contest` (
  `cid` int(11) NOT NULL AUTO_INCREMENT,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `cname` varchar(64) DEFAULT NULL,
  `freeze` int(11) NOT NULL DEFAULT '0',
  `result` enum('yes','no') NOT NULL DEFAULT 'yes',
  `owner` varchar(16) NOT NULL,
  PRIMARY KEY (`cid`),
  UNIQUE KEY `cid` (`cid`),
  KEY `cid_2` (`cid`),
  KEY `owner` (`owner`),
  KEY `end_time` (`end_time`),
  KEY `start_time` (`start_time`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `contest_coowner`
--

CREATE TABLE IF NOT EXISTS `contest_coowner` (
  `id` varchar(30) DEFAULT NULL,
  `cid` int(13) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `coowner`
--

CREATE TABLE IF NOT EXISTS `coowner` (
  `id` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE IF NOT EXISTS `faq` (
  `qid` int(11) NOT NULL DEFAULT '0',
  `question` text CHARACTER SET utf8,
  `answer` text CHARACTER SET utf8,
  PRIMARY KEY (`qid`),
  UNIQUE KEY `qid` (`qid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hashcode_pid`
--

CREATE TABLE IF NOT EXISTS `hashcode_pid` (
  `hashcode` varchar(50) NOT NULL,
  `pid` int(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `icpc_sid`
--

CREATE TABLE IF NOT EXISTS `icpc_sid` (
  `ssid` int(11) NOT NULL,
  `icpc_sid` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `judge`
--

CREATE TABLE IF NOT EXISTS `judge` (
  `id` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mapping`
--

CREATE TABLE IF NOT EXISTS `mapping` (
  `pid` int(11) NOT NULL,
  `realid` char(16) NOT NULL,
  PRIMARY KEY (`pid`),
  KEY `pid` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE IF NOT EXISTS `message` (
  `mid` int(11) NOT NULL,
  `cid` int(11) NOT NULL,
  `from` varchar(16) NOT NULL,
  `to` varchar(16) NOT NULL,
  `info` text,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `flag` tinyint(1) NOT NULL,
  PRIMARY KEY (`mid`),
  KEY `to` (`to`),
  KEY `cid` (`cid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pid_cid`
--

CREATE TABLE IF NOT EXISTS `pid_cid` (
  `pid` int(11) NOT NULL,
  `cid` int(11) NOT NULL,
  KEY `pid` (`pid`),
  KEY `cid` (`cid`),
  KEY `pid_2` (`pid`),
  KEY `cid_2` (`cid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `problems`
--

CREATE TABLE IF NOT EXISTS `problems` (
  `pid` int(11) NOT NULL DEFAULT '0',
  `pname` varchar(64) CHARACTER SET utf8 DEFAULT NULL,
  `cid` int(11) DEFAULT NULL,
  `memory_limit` int(11) DEFAULT NULL,
  `time_limit` int(11) DEFAULT NULL,
  `description` text CHARACTER SET utf8,
  `input` text CHARACTER SET utf8,
  `output` text CHARACTER SET utf8,
  `sample_input` text CHARACTER SET utf8,
  `sample_output` text CHARACTER SET utf8,
  `problemsetter` varchar(32) DEFAULT NULL,
  `anonymous` char(8) DEFAULT '0',
  `special_judge` char(4) NOT NULL DEFAULT '',
  `gid` int(11) DEFAULT NULL,
  `tid` int(11) NOT NULL DEFAULT '0',
  `parent_pid` int(11) NOT NULL DEFAULT '0',
  `visible` varchar(10) NOT NULL DEFAULT 'unchecked',
  PRIMARY KEY (`pid`),
  UNIQUE KEY `pid` (`pid`),
  KEY `gid` (`gid`),
  KEY `pid_2` (`pid`),
  KEY `pid_3` (`pid`),
  KEY `pid_4` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `submissions`
--

CREATE TABLE IF NOT EXISTS `submissions` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `uid` varchar(16) DEFAULT NULL,
  `pid` int(11) NOT NULL,
  `status` varchar(30) DEFAULT NULL,
  `cpu` decimal(6,3) DEFAULT NULL,
  `memory` int(11) DEFAULT NULL,
  `source` char(10) CHARACTER SET latin1 NOT NULL,
  `err_msg` blob,
  `SSID` int(11) NOT NULL,
  PRIMARY KEY (`sid`),
  UNIQUE KEY `sid` (`sid`),
  KEY `uid` (`uid`,`pid`),
  KEY `sid_2` (`sid`),
  KEY `sid_3` (`sid`),
  KEY `pid` (`pid`),
  KEY `uid_2` (`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `submission_result_detail`
--

CREATE TABLE IF NOT EXISTS `submission_result_detail` (
  `sid` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `tid` int(11) NOT NULL,
  `verdict` varchar(30) NOT NULL,
  `runTime` float NOT NULL,
  `memoryAmt` int(11) NOT NULL,
  `errMsg` text CHARACTER SET utf8 NOT NULL,
  KEY `sid` (`sid`),
  KEY `sid_2` (`sid`),
  KEY `pid` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `testcases`
--

CREATE TABLE IF NOT EXISTS `testcases` (
  `tid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `timeLimit` int(11) NOT NULL,
  `memoryLimit` int(11) NOT NULL,
  `description` tinytext CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`tid`),
  UNIQUE KEY `tid` (`tid`),
  KEY `tid_2` (`tid`),
  KEY `pid` (`pid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` varchar(16) CHARACTER SET latin1 NOT NULL,
  `password` char(41) CHARACTER SET latin1 DEFAULT NULL,
  `real_name` varchar(16) DEFAULT NULL,
  `email` varchar(64) CHARACTER SET latin1 NOT NULL,
  `user_level` int(11) NOT NULL,
  `nickname` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `id_2` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `uva_sid`
--

CREATE TABLE IF NOT EXISTS `uva_sid` (
  `ssid` int(11) NOT NULL,
  `uva_sid` bigint(20) NOT NULL,
  PRIMARY KEY (`ssid`),
  KEY `sid` (`ssid`),
  KEY `uva_sid` (`uva_sid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

insert into users values('boss',password('boss'),'boss','NULL',100,'boss');
insert into coowner values('boss');
insert into judge values('boss');
insert into admin values('boss');

exit
