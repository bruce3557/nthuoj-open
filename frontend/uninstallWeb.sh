#!/bin/bash

# check sudo privileges

user=$(whoami)
if [ $user != "root" ]; then
	echo "Permission denied, are you root?">&2
	exit
fi


# remove nthuoj folder

if [ -d /etc/nthuoj ]; then
	rm -rf /etc/nthuoj
fi


#  remove web folder

if [ -d /var/www/nthuoj ]; then
	rm -rf /var/www/nthuoj
fi


# drop nthuoj database

read -p "Please enter your Mysql user: " sqlUser
mysql -u $sqlUser -p  -e "DROP DATABASE nthuoj"
if [ $? != 0 ]; then
	echo "Database drop failed, please check your password">&2
	exit
fi

echo "Uninstall completed!">&2
