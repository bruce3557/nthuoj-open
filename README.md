NTHUOJ
=======
NTHU OJ is an online judge project currently maintained by users in National Tsing Hua University (Taiwan).
This project provides an relatively simple structure of a real , complicated online judge system . 
Currently , we only support C / C++ code sumission and judging, and we're working on JAVA .

Though this system is originally developed aimed for ACM-ICPC competitors training , it can also be used for other educational purpose(Homework or Exam , as long as they have fixed answers).

Our project consists of three parts: Frontend, Backend, and Frontend/Backend (Dispatcher).
Frontend is mainly about web pages. Backend is the place that deal with the judge process. Frontend/Backend part is a dispatcher that schedule the judge works to backend vms.

##Install Guide
---
###Get our project:
    git clone https://bitbucket.org/bruce3557/nthuoj-open
---
####Dependencies: 
* openssh-server 
* php5 
* apache2 
* nfs-kernel-server 
* php5-mysql 
* mysql-server
* phpmyadmin
* g++-4.7+ 
* gcc-4.7+
* php5-curl

---
###Frontend:
---
* Copy our files the the web display folder: (For convinience, all files are in the 'frontend/web/' folder. You can just replace the initial 'www' folder.)

    ```
	sudo cp -r frontend/* /var  ( or any folder one directory above 'www') 
    ```	

* Edit nthuoj.ini to change the ip , username and password of the database engine.  

* And, start running:

    ```	
	sudo chmod +x setup.sh
	sudo ./setup.sh
    ```	

---
###Frontend/Backend (Dispatcher):
---

* move ‘dispatcher’ folder to places you like. ex.

    ```	
	sudo cp -r dispatcher/* /var  (or any directory you prefer)
    ```
	
	
* write ‘machine.cofig’ (under ‘dispatcher’ folder):
Write this file to indicate the judge vms’ name and their ports . abide by the following format:

    ```	
	[vm name] [port]
    ```	
    
    ex
 

    
	
        nthuoj-vm1 192.168.136.101 
   
        nthuoj-vm2 192.168.136.102   
   
        nthuoj-newoj 192.168.136.103   
    
   	
	

* run dispatcher: go to ‘dispatcher’ folder and:

    ```	
	nohup php dispatcher.php & ('nohup' is used to make sure dispatcher keeps working even the terminal is closed.)
    ```
	
---
###Backend:
---

* Run the install.sh(in backend/VM) on the VM which you want to use as a Judge VM and follow the instructions to type some necessary information. 

    ```
    sudo ./install.sh
    ```	

* Mount the source, testcase, and speJudge folders in judge VM to the corresponding folders in dispatcher VM. 

    * Give some IP to mount the files on dispatcher VM. Add a file called 'exports' in /etc on dispatcher VM with the content:

        ```    
        [folder path saved in dispatcher VM] [Judge VM IP](rw,sync,no_subtree_check)
        ```
        ex:
    
        ```    
        /home/acm/Documents/nthuoj/judge/source 192.168.136.2(rw,sync,no_subtree_check)
        ```
        
    * Reboot dispatcher VM.

    * Give commands in every Judge VM to mount the folders:
	
        ```
        mount -t nfs [dispatcher IP]:[folder path saved in dispatcher VM] [folder path saved in judge VM]
        ```
        ex

        ```
        mount -t nfs 192.168.136.1:/home/acm/Documents/nthuoj/judge/source /home/nthuoj/judge/source
        ```

* For connection to other judges such as UVA, complete the config files: 

    ```
    uva.config,
    icpc.config,
    pku.config
    (inside the 'outsideConnection' folder)
    ```
    
Take uva.config for example. Put your user name, password and uhunt id in the corresponding places in the uva.config file.

* For outsideconnection, run moniteruva.php posticpc.php crawlicpc.php on dispatcher VM: 

    ```
	sudo php moniteruva.php&
    ```
    ```
	sudo php posticpc.php&
    ```	
    ```
        sudo php crawlicpc.php&
    ```