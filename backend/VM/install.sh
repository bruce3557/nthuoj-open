#!/bin/bash
echo "Start install"
echo "Please enter the directory path (absolute path) where you want to install:"
read dirToInstall 

while [ "$dirToInstall" == "" -o ! -d "$dirToInstall"  ]
do
	echo "the directory does not exist"
	read -p "please enter the path again: " dirToInstall
	
done

if [ -d $dirToInstall/nthuoj ]; then
	echo "NTHUOJ has been install. Please remove it first"
	exit
fi
cd $(dirname $0)

echo "Create folders"
mkdir $dirToInstall/nthuoj
mkdir $dirToInstall/nthuoj/mountServer
mkdir $dirToInstall/nthuoj/mountServer/source
mkdir $dirToInstall/nthuoj/mountServer/speJudge
mkdir $dirToInstall/nthuoj/mountServer/testdata
mkdir $dirToInstall/nthuoj/judge
mkdir $dirToInstall/nthuoj/judgeFile
mkdir $dirToInstall/nthuoj/judgeFile/testdata
mkdir $dirToInstall/nthuoj/log

if [ ! -d /etc/nthuoj ]; then
	mkdir /etc/nthuoj
fi

echo "finish creating folders"
echo "move files to the directory"
cp original.config $dirToInstall/nthuoj
cp genConfig.sh $dirToInstall/nthuoj

cp www/interface.php /var/www/interface.php
cp www/interfaceFunc.php /var/www/interfaceFunc.php

cp judge/blacklist $dirToInstall/nthuoj/judge
cp judge/judge.sh $dirToInstall/nthuoj/judge
cp judge/runcode.sh $dirToInstall/nthuoj/judge

echo "finish moving"
chmod +x $dirToInstall/nthuoj/genConfig.sh
chmod +x $dirToInstall/nthuoj/judge/judge.sh
chmod +x $dirToInstall/nthuoj/judge/runcode.sh
chown -R www-data:www-data $dirToInstall/nthuoj/judgeFile
chown -R www-data:www-data $dirToInstall/nthuoj/log
chown -R www-data:www-data $dirToInstall/nthuoj/mountServer

echo "Start genConfig"
$dirToInstall/nthuoj/genConfig.sh

echo "Install finish"
