#!/bin/bash

LOG="/var/nthuoj/log/outsideConnectionLog"
$JUDGE_START_TIME
echo "" >> $LOG
echo $JUDGE_START_TIME >> $LOG
`date` >> $LOG
echo "OUTSIDE_JUDGE_START" >> $LOG
if [ $# -lt 4 ];then
	echo "usage: ./sendToOtherJudge code_type problem_id otherjudge_id submit_id time_limit"
	echo "argument arror" >> $LOG
	exit
fi

cd $(dirname $0)

CODE_TYPE=$1
PID=$2
OTHER_JUDGE=$3
SUBMIT_ID=$4

ERROR="errMsg"
POSTPKU="./postpku.php"
POSTUVA="./postuva.php"
POSTZOJ="./postzoj.php"
STEALPKU="./stealpku.php"
STEALZOJ="./stealzoj.php"
USRCODE="/var/nthuoj/source/$SUBMIT_ID"."$CODE_TYPE"
echo $USRCODE

#if [ $OTHER_JUDGE -eq 1 ];then
	#Do nothing, icpc judging is monitored by posticpc.php 
	#and crawlicpc.php which are always running in background
if [ $OTHER_JUDGE -eq 2 ];then
	echo "POJ" >> $LOG
	if [ $CODE_TYPE = "cpp" ];then
		$POSTPKU $USRCODE $PID 0
	else 
		$POSTPKU $USRCODE $PID 1
	fi
	$STEALPKU $SUBMIT_ID 2>>$ERROR 1>>$LOG
	
elif [ $OTHER_JUDGE -eq 3 ];then
	echo "UVA" >> $LOG
	if [ $CODE_TYPE = "cpp" ];then
		$POSTUVA $USRCODE $PID 3 $SUBMIT_ID
	else 
		$POSTUVA $USRCODE $PID 1 $SUBMIT_ID
	fi

elif [ $OTHER_JUDGE -eq 4 ];then
	echo "ZOJ" >> $LOG
	if [ $CODE_TYPE = "cpp" ];then
		$POSTZOJ $USRCODE $PID 2
	else 
		$POSTZOJ $USRCODE $PID 1
	fi
	$STEALZOJ $SUBMIT_ID 2>>$ERROR 1>>$LOG
fi



